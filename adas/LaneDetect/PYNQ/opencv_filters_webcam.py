
from pynq import Overlay
Overlay("base.bit").download()
from pynq.drivers import HDMI
from pynq.drivers.video import VMODE_640x480

hdmi_out = HDMI('out')
hdmi_out.start()
hdmi_out.mode(VMODE_640x480)
hdmi_out.start()

# monitor (output) frame buffer size
frame_out_w = 1920
frame_out_h = 1080
# camera (input) configuration
frame_in_w = 640
frame_in_h = 480

from pynq.drivers import Frame
import cv2

videoIn = cv2.VideoCapture(0)
videoIn.set(cv2.CAP_PROP_FRAME_WIDTH, frame_in_w);
videoIn.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_in_h);

print("capture device is open: " + str(videoIn.isOpened()))


import numpy as np

ret, frame_vga = videoIn.read()

if (ret):
    frame_1080p = np.zeros((1080,1920,3)).astype(np.uint8)
    frame_1080p[0:480,0:640,:] = frame_vga[0:480,0:640,:]
    hdmi_out.frame_raw(bytearray(frame_1080p.astype(np.int8).tobytes()))
else:
    raise RuntimeError("Error while reading from camera.")

import time
frame_1080p = np.zeros((1080,1920,3)).astype(np.uint8)

num_frames = 200
readError = 0

start = time.time()
for i in range (num_frames):   
    # read next image
    ret, frame_vga = videoIn.read()
    if (ret):
        laplacian_frame = cv2.Laplacian(frame_vga, cv2.CV_8U)
        # copy to frame buffer / show on monitor reorder RGB (HDMI = GBR)
        frame_1080p[0:480,0:640,[0,1,2]] = laplacian_frame[0:480,0:640,
                                                           [1,0,2]]
        hdmi_out.frame_raw(bytearray(frame_1080p.astype(np.int8).tobytes()))
    else:
        readError += 1
end = time.time()

print("Frames per second: " + str((num_frames-readError) / (end - start)))
print("Number of read errors: " + str(readError))


frame_1080p = np.zeros((1080,1920,3)).astype(np.uint8)

num_frames = 20

start = time.time()
for i in range (num_frames):
    # read next image
    ret, frame_webcam = videoIn.read()
    if (ret):
        frame_canny = cv2.Canny(frame_webcam,100,110)
        frame_1080p[0:480,0:640,0] = frame_canny[0:480,0:640]
        frame_1080p[0:480,0:640,1] = frame_canny[0:480,0:640]
        frame_1080p[0:480,0:640,2] = frame_canny[0:480,0:640]
        # copy to frame buffer / show on monitor
        hdmi_out.frame_raw(bytearray(frame_1080p.astype(np.int8).tobytes()))
    else:
        readError += 1
end = time.time()

print("Frames per second: " + str((num_frames-readError) / (end - start)))
print("Number of read errors: " + str(readError))


get_ipython().magic('matplotlib inline')
from matplotlib import pyplot as plt
import numpy as np

plt.figure(1, figsize=(10, 10))
frame_vga = np.zeros((480,640,3)).astype(np.uint8)
frame_vga[0:480,0:640,0] = frame_canny[0:480,0:640]
frame_vga[0:480,0:640,1] = frame_canny[0:480,0:640]
frame_vga[0:480,0:640,2] = frame_canny[0:480,0:640]
plt.imshow(frame_vga[:,:,[2,1,0]])
plt.show()

videoIn.release()
hdmi_out.stop()
del hdmi_out



