
from pynq import Overlay
Overlay("base.bit").download()

from pynq.drivers import HDMI
from pynq.drivers.video import VMODE_1920x1080
hdmi_out = HDMI('out',video_mode=VMODE_1920x1080)
hdmi_in = HDMI('in', frame_list=hdmi_out.frame_list)
hdmi_in.start()
hdmi_out.start()


from IPython.display import Image
frame = hdmi_in.frame()
orig_img_path = '/home/xilinx/jupyter_notebooks/examples/' +                 'data/opencv_filters.jpg'
frame.save_as_jpeg(orig_img_path)

Image(filename=orig_img_path)


hdmi_in.frame_index_next()


import time
import cv2
import numpy as np

num_frames = 20

start = time.time()

for i in range (num_frames):    
    np_frame= (np.frombuffer(hdmi_in.frame_raw(), 
                             dtype=np.uint8)).reshape(1080,1920,3)
    laplacian_frame = cv2.Laplacian(np_frame, cv2.CV_8U) 
    hdmi_out.frame_raw(bytearray(laplacian_frame.tobytes()))
    
end = time.time()

print("Frames per second:  " + str((num_frames) / (end - start)))


orig_img_path = '/home/xilinx/jupyter_notebooks/examples/' +                 'data/opencv_filters.jpg'
hdmi_out.frame().save_as_jpeg(orig_img_path)

Image(filename=orig_img_path)


import time
import cv2
import numpy as np

num_frames = 20

start = time.time()

for i in range (num_frames):
    # read next image
    np_frame= (np.frombuffer(hdmi_in.frame_raw(), 
                             dtype=np.uint8)).reshape(1080,1920,3)
    frame_canny = cv2.Canny(np_frame,100,110)
    np_frame[:,:,0] = frame_canny[:,:]
    np_frame[:,:,1] = frame_canny[:,:]
    np_frame[:,:,2] = frame_canny[:,:]

    # copy to frame buffer / show on monitor
    hdmi_out.frame_raw(bytearray(np_frame.tobytes()))

end = time.time()
print("Frames per second:  " + str((num_frames) / (end - start)))


hdmi_out.stop()
hdmi_in.stop()
del hdmi_in, hdmi_out

