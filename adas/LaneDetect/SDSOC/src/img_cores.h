#ifndef MANR_H
#define MANR_H
//#ifdef __cplusplus
//extern "C" {
//#endif

#pragma SDS data mem_attribute(yc_in:PHYSICAL_CONTIGUOUS, yc_out:PHYSICAL_CONTIGUOUS)
#pragma SDS data access_pattern(yc_in:SEQUENTIAL, yc_out:SEQUENTIAL)
//void sobel_filter ( unsigned char *yc_in, unsigned char *yc_out);
extern void sobel_filter(unsigned char yc_in[NUMROWS*NUMCOLS],unsigned char yc_out[NUMROWS*NUMCOLS]);

//#ifdef __cplusplus
//};
//#endif
#endif 
