/******************************************************************************
 *
 * (c) Copyright 2012 Xilinx, Inc. All rights reserved.
 *
 * This file contains confidential and proprietary information of Xilinx, Inc.
 * and is protected under U.S. and international copyright and other
 * intellectual property laws.
 *
 * DISCLAIMER:
 * This disclaimer is not a license and does not grant any rights to the
 * materials distributed herewith. Except as otherwise provided in a valid
 * license issued to you by Xilinx, and to the maximum extent permitted by
 * applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL
 * FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS,
 * IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF
 * MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE;
 * and (2) Xilinx shall not be liable (whether in contract or tort, including
 * negligence, or under any other theory of liability) for any loss or damage
 * of any kind or nature related to, arising under or in connection with these
 * materials, including for any direct, or any indirect, special, incidental,
 * or consequential loss or damage (including loss of data, profits, goodwill,
 * or any type of loss or damage suffered as a result of any action brought by
 * a third party) even if such damage or loss was reasonably foreseeable or
 * Xilinx had been advised of the possibility of the same.
 *
 * CRITICAL APPLICATIONS:
 * Xilinx products are not designed or intended to be fail-safe, or for use in
 * any application requiring fail-safe performance, such as life-support or
 * safety devices or systems, Class III medical devices, nuclear facilities,
 * applications related to the deployment of airbags, or any other applications
 * that could lead to death, personal injury, or severe property or
 * environmental damage (individually and collectively, "Critical
 * Applications"). Customer assumes the sole risk and liability of any use of
 * Xilinx products in Critical Applications, subject only to applicable laws
 * and regulations governing limitations on product liability.
 *
 * THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE
 * AT ALL TIMES.
 *
 *******************************************************************************/

/*****************************************************************************
 *
 * @file main_app.c
 *
 * Implementation of controlling application.
 *
 ******************************************************************************/

#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/types_c.h>
#include <hls_video.h>
#include <hls_opencv.h>


#include <iostream> // for standard I/O
#include <string>   // for strings

#include <sds_lib.h>

#include "frame_size.h"
#include "img_cores.h"

#define TIME_STAMP_INIT  unsigned long long clock_start, clock_end,ts ;  clock_start = sds_clock_counter();
#define TIME_STAMP  { clock_end = sds_clock_counter(); ts= clock_end-clock_start; clock_start = sds_clock_counter();}

using namespace cv;
using namespace std;

static void help()
{
    std::cout
        << "------------------------------------------------------------------------------" << endl
        << "Usage:"                                                                         << endl
        << "./sobel inputvideoName"                                                       << endl
        << "------------------------------------------------------------------------------" << endl
        << endl;
}

int main(int argc, char *argv[])
{

	//unsigned char* inarray;
	//unsigned char outarray[NUMROWS*NUMCOLS]= {0};
    bool image_out =false;
    int frame_cnt=0,avg_ts=0,sum_ts=0;
	unsigned char *inarray = (unsigned char *)sds_alloc(NUMROWS*NUMCOLS);
	unsigned char *outarray = (unsigned char *)sds_alloc(NUMROWS*NUMCOLS);

	help();

	if (argc != 2)
	{
		cout << "Not enough parameters" << endl;
		return -1;
	}
	const string source = argv[1];           // the source file name

	cv::VideoCapture input_cap(source);              // Open input

	const string dest = "outVideo.avi";

	if (!input_cap.isOpened())
	{
		cout  << "Could not open the input video: " << source << endl;
		return -1;
	}

	// Setup output video
	cv::VideoWriter output_cap;
    int codec = CV_FOURCC('M', 'J', 'P', 'G');  // select desired codec (must be available at runtime)
	output_cap.open(dest,
	                codec,
	                input_cap.get(CV_CAP_PROP_FPS),
	                cv::Size(NUMCOLS,
	                NUMROWS),1);

	if (!output_cap.isOpened())
	{
	        std::cout << "!!! Output video could not be opened" << std::endl;
		    image_out = true;
	}

    TIME_STAMP_INIT

	for(;;)
    {
        cv::Mat frame;

        input_cap >> frame; // get a new frame from camera


        if (!frame.empty())
        {
          cv::Mat resized,colorFrame;
          cv::Mat gray_mat(NUMROWS, NUMCOLS, CV_8UC1, inarray);
          cv::resize(frame, resized, cv::Size(NUMCOLS,NUMROWS));
          cv::cvtColor(resized, gray_mat, COLOR_BGR2GRAY);
          //memcpy(inarray,gray_mat.data,NUMCOLS*NUMROWS);
          //std::cout << "!!! Entering HW hold on ..." << std::endl;

          TIME_STAMP
       	  sobel_filter(gray_mat.data,outarray);
          TIME_STAMP

          frame_cnt++;
          sum_ts +=ts;
          avg_ts =sum_ts/frame_cnt;
          std::cout << ".";


          //std::cout << "!!! Returned from HW yoooo hoo !!..." << std::endl;
          cv::Mat sobel_mat = cv::Mat(NUMROWS, NUMCOLS, CV_8UC1, outarray);
          cv::cvtColor(sobel_mat, colorFrame, CV_GRAY2BGR);

          if(image_out)
          {
              std::cout << "!!! One time write of output to an image file !!..." << std::endl;
              imwrite( "gray_Image.jpg", gray_mat );
              imwrite( ".sobel_Image.jpg", sobel_mat );
        	  break;
          }
          else
          {
        	  output_cap.write(colorFrame);
          }
        }
        else
        {
        	std::cout<<"No input frame!"<<std::endl;
        	break;
        }

    }

    printf("Total number of frames = %d, Average time per call = %d",frame_cnt,avg_ts);

	input_cap.release();
	output_cap.release();

	return 0;
}


