// Include the GSM library
#include <GSM.h>


#include "NAxisMotion.h"        //Contains the bridge code between the API and the Arduino Environment
#include <Wire.h>


#define SMS


#define PINNUMBER ""
#define MAX_MESSAGES (100)

#define FATAL_ACC_LEVEL   (30)
#define WARNING_ACC_LEVEL (20)

// initialize the library instance
GSM gsmAccess;
GSM_SMS sms;

NAxisMotion mySensor;         //Object that for the sensor 

char remoteNum[20] = "8587055407";  // telephone number to send sms
char txtMsg_warning[100]   = "!!! Automated WARNING text: Sensed an unsafe manouver of the subscribed vehicle !!!"; 
char txtMsg_fatal[100]     = "!!! Automated WARNING text: Sensed a FATAL manouver of the subscribed vehicle !!!"; 

unsigned long lastStreamTime = 0;     //To store the last streamed time stamp
const int streamPeriod = 40;          //To stream at 25Hz without using additional timers (time period(ms) =1000/frequency(Hz))
bool updateSensorData = true;         //Flag to update the sensor data. Default is true to perform the first read before the first stream

void setup() {
  // initialize serial communications and wait for port to open:
  Serial.begin(9600);

  I2C.begin();                    //Initialize I2C communication to the let the library communicate with the sensor.
  //Sensor Initialization
  mySensor.initSensor();          //The I2C Address can be changed here inside this function in the library
  mySensor.setOperationMode(OPERATION_MODE_NDOF);   //Can be configured to other operation modes as desired
  mySensor.setUpdateMode(MANUAL);  //The default is AUTO. Changing to MANUAL requires calling the relevant update functions prior to calling the read functions
  //Setting to MANUAL requires fewer reads to the sensor  

  //Setting to MANUAL requires lesser reads to the sensor
  mySensor.updateAccelConfig();
  updateSensorData = true;

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println();
  Serial.println("Default accelerometer configuration settings...");
  Serial.print("Range: ");
  Serial.println(mySensor.readAccelRange());
  Serial.print("Bandwidth: ");
  Serial.println(mySensor.readAccelBandwidth());
  Serial.print("Power Mode: ");
  Serial.println(mySensor.readAccelPowerMode());
  Serial.println("Streaming in ..."); //Countdown
  Serial.print("3...");
  delay(1000);  //Wait for a second
  Serial.print("2...");
  delay(1000);  //Wait for a second
  Serial.println("1...");
  delay(1000);  //Wait for a second


  Serial.println("SMS Messages Sender");

#ifdef SMS

  // connection state
  boolean notConnected = true;

  // Start GSM shield
  // If your SIM has PIN, pass it as a parameter of begin() in quotes
  while (notConnected) {
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }
#endif

  Serial.println("GSM initialized");
  Serial.println("\nIn case of emergency SMS will be sent to ");
  Serial.println(remoteNum);

}

void loop() {

  float lx,ly,lz;
  char* txt_ptr = NULL;
  static unsigned int msg_count = 0;
  if (updateSensorData)  //Keep the updating of data as a separate task
  {
    mySensor.updateAccel();        //Update the Accelerometer data
    mySensor.updateLinearAccel();  //Update the Linear Acceleration data
    mySensor.updateGravAccel();    //Update the Gravity Acceleration data
    mySensor.updateCalibStatus();  //Update the Calibration Status
    updateSensorData = false;
  }

  if ((millis() - lastStreamTime) >= streamPeriod)
  {
    lastStreamTime = millis();    
    mySensor.updateEuler();        //Update the Euler data into the structure of the object
    mySensor.updateCalibStatus();  //Update the Calibration Status

    mySensor.updateAccel();        //Update the Accelerometer data
    mySensor.updateLinearAccel();  //Update the Linear Acceleration data
    mySensor.updateGravAccel();    //Update the Gravity Acceleration data
    mySensor.updateCalibStatus();  //Update the Calibration Status
    

    Serial.print("Orientation: ");
    Serial.print(mySensor.readEulerHeading());
    Serial.print(" ");
    Serial.print(mySensor.readEulerPitch());
    Serial.print(" ");
    Serial.println(mySensor.readEulerRoll());

    lx = mySensor.readLinearAccelX();
    ly = mySensor.readLinearAccelY();
    lz = mySensor.readLinearAccelZ();

/*
    Serial.print("lX: ");
    Serial.print(lx); //Linear Acceleration X-Axis data
    Serial.print("m/s2 ");

    Serial.print(" lY: ");
    Serial.print(ly);  //Linear Acceleration Y-Axis data
    Serial.print("m/s2 ");

    Serial.print(" lZ: ");
    Serial.print(lz);  //Linear Acceleration Z-Axis data
    Serial.print("m/s2 ");
*/
    Serial.println();


    if((abs(lx) > FATAL_ACC_LEVEL || abs(ly) > FATAL_ACC_LEVEL || abs(lz) > FATAL_ACC_LEVEL)){
    txt_ptr = &txtMsg_fatal[0];
    msg_count++;
    }
    else if((abs(lx) > WARNING_ACC_LEVEL || abs(ly) > WARNING_ACC_LEVEL || abs(lz) > WARNING_ACC_LEVEL)){
    txt_ptr = &txtMsg_warning[0];
    msg_count++;
    }

    if(txt_ptr && msg_count <= MAX_MESSAGES){      
    Serial.println("\r\nSENDING");
    Serial.println();
    Serial.println("Message:");
    Serial.println(txt_ptr);

#ifdef SMS
    // send the message
    sms.beginSMS(remoteNum);
    sms.print(txt_ptr);
    sms.endSMS();
    Serial.println("\nCOMPLETE!\n");
#endif

    }
    updateSensorData = true;

  }

}

/*
  Read input serial
 */
int readSerial(char result[]) {
  int i = 0;
  while (1) {
    while (Serial.available() > 0) {
      char inChar = Serial.read();
      if (inChar == '\n') {
        result[i] = '\0';
        Serial.flush();
        return 0;
      }
      if (inChar != '\r') {
        result[i] = inChar;
        i++;
      }
    }
  }
}


