
#include "sobel.h"
#include "math.h"
#include <stdlib.h>


int Gx, Gy;
BYTE windowBuffer [3][3];

void sobel(BYTE input_image[IMG_H][IMG_W],BYTE output_image[IMG_H][IMG_W])
{
	/*
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=input_image
#pragma HLS INTERFACE axis port=output_image
#pragma HLS ARRAY_RESHAPE variable=windowBuffer complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer complete dim=1
#pragma HLS ARRAY_PARTITION variable=lineBuffer complete dim=1
*/
	BYTE tmp;

for (int i=0; i<IMG_H; i++) //1 to img_h-2
{
	line_buffer_shift:for (int j=0; j<(IMG_W); j++) //1 to img_w-2
	{
		if(i==0 || j==0 || i==IMG_H-1 || j==IMG_W-1) {
					output_image[i][j] = 0;
				}
		else
		{

		//#pragma HLS PIPELINE
		windowBuffer[0][0] = input_image[i-1][j-1];
		windowBuffer[0][1] = input_image[i-1][j];
		windowBuffer[0][2] = input_image[i-1][j+1];

		windowBuffer[1][0] = input_image[i][j-1];
		windowBuffer[1][1] = input_image[i][j];
		windowBuffer[1][2] = input_image[i][j+1];

		windowBuffer[2][0] = input_image[i+1][j-1];
		windowBuffer[2][1] = input_image[i+1][j];
		windowBuffer[2][2] = input_image[i+1][j+1];

		// Calculating Gx and Gy for pixel[i][j]
		Gx = (windowBuffer[2][0]+windowBuffer[2][1] + windowBuffer[2][1] +windowBuffer[2][2])-(windowBuffer[0][0]+windowBuffer[0][1]  +windowBuffer[0][1] +windowBuffer[0][2]);
		Gy = (windowBuffer[0][2]+windowBuffer[1][2] + windowBuffer[1][2] +windowBuffer[2][2])-(windowBuffer[0][0]+windowBuffer[1][0]  + windowBuffer[1][0]+windowBuffer[2][0]);

			tmp = (abs(Gx) + abs(Gy));
			output_image[i][j] = tmp;
		}
	}
}
}
