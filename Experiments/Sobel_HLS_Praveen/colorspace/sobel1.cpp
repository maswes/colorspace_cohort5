
#include "sobel.h"
#include "math.h"
#include <stdlib.h>


int Gx, Gy;
BYTE lineBuffer [3][IMG_W];
BYTE windowBuffer [3][3];

void sobel(hls::stream<BYTE>& input_image, hls::stream<BYTE>& output_image)
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=input_image
#pragma HLS INTERFACE axis port=output_image
#pragma HLS ARRAY_RESHAPE variable=windowBuffer complete dim=1
#pragma HLS ARRAY_PARTITION variable=windowBuffer complete dim=1
#pragma HLS ARRAY_PARTITION variable=lineBuffer complete dim=1

	BYTE tmp;

for (int i=0; i<IMG_H; i++) //1 to img_h-2
{
	line_buffer_shift:for (int j=0; j<(IMG_W); j++) //1 to img_w-2
	{
		#pragma HLS PIPELINE
		lineBuffer [0][j] = lineBuffer [1][j] ;
		lineBuffer [1][j] = lineBuffer [2][j] ;
		input_image.read(tmp);
		lineBuffer [2][j] = tmp;

		windowBuffer[0][0] = lineBuffer	[0][j];
		windowBuffer[1][0] = lineBuffer	[1][j];
		windowBuffer[2][0] = lineBuffer	[2][j];

		window_shift:for	(int k = 0; k < 3; k++) {
			windowBuffer[k][2] = windowBuffer[k][1];
			windowBuffer[k][1] = windowBuffer[k][0];
		}

		// Calculating Gx and Gy for pixel[i][j]
		Gx = (windowBuffer[2][0]+windowBuffer[2][1] + windowBuffer[2][1] +windowBuffer[2][2])-(windowBuffer[0][0]+windowBuffer[0][1]  +windowBuffer[0][1] +windowBuffer[0][2]);
		Gy = (windowBuffer[0][2]+windowBuffer[1][2] + windowBuffer[1][2] +windowBuffer[2][2])-(windowBuffer[0][0]+windowBuffer[1][0]  + windowBuffer[1][0]+windowBuffer[2][0]);

		if(i==0 || j==0 || i==IMG_H-1 || j==IMG_W-1) {
			output_image.write(tmp);
		}
		else {
			tmp = (abs(Gx) + abs(Gy));
			output_image.write(tmp);
		}
	}
}
}
