#include "hls_solbel.h"

//void image_filter(AXI_STREAM& video_in, AXI_STREAM& video_out, int rows, int cols)
void image_filter(AXI_STREAM& video_in, AXI_STREAM& video_out)
{
	//Create AXI streaming interfaces for the core
	#pragma HLS INTERFACE axis port=video_in bundle=INPUT_STREAM
	#pragma HLS INTERFACE axis port=video_out bundle=OUTPUT_STREAM
	//#pragma HLS INTERFACE s_axilite port=rows bundle=CONTROL_BUSoffset=0x14
	//#pragma HLS INTERFACE s_axilite port=cols bundle=CONTROL_BUSoffset=0x1C
	//#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL_BUS

	//#pragma HLS INTERFACE ap_stable port=rows
	//#pragma HLS INTERFACE ap_stable port=cols
	int rows = 512;
	int cols =512;
	RGB_IMAGE img_0(rows, cols);
	RGB_IMAGE img_1(rows, cols);
	#pragma HLS dataflow
	AXIvideo2Mat(video_in, img_0);
	Sobel<1,0,3>(img_0, img_1);
	Mat2AXIvideo(img_1, video_out);
}
