#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <hls_opencv.h>
#include <ctime>

#include "sobel.h"


FILE* file;
FILE* writeFile;

int main()
{


	BYTE      input_image[IMG_H][IMG_W];
	BYTE      gaus_image[IMG_H][IMG_W];
	BYTE      output_image[IMG_H][IMG_W];
	BYTE      opencv_output_image[IMG_H][IMG_W];

	int i,j, n;
	n=1;
	for (i=0; i<IMG_H; i++)
	{
		for (j=0; j<IMG_W; j++)
		{
			input_image[i][j] = 0;//GetRand(0, 255);
			output_image[i][j] = input_image[i][j];
			n=n+1;	
		}
	}
		
		file = fopen("lena_gray.raw", "rb");
		
		if (!file)
		{
		printf("Something wrong in reading image file\n");
		}
		else
		{
			fread(input_image, sizeof(char),IMAGE_SIZE, file);
			memcpy(output_image, input_image,sizeof(char) * IMAGE_SIZE);
			fclose(file);
			printf("Image file read success!\n");
		}
		

		//Our Sobel Function
		int ret=gaussian(input_image, gaus_image); //gaussian smoothing

		//for comparing performance
		clock_t start;
		double duration;
		start = std::clock();
		sobel(gaus_image, output_image); //calculating sobel

		//Writing Our Sobel filtered Image
		writeFile = fopen("lena_gray_out.raw", "wb");
		fwrite(gaus_image, sizeof(char), IMAGE_SIZE, writeFile);
		fclose(writeFile);


		//OpenCV Sobel Function: Apply sobel filter and save image as jpg
		opencv_sobel(gaus_image);

		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
		std::cout<<"Our Sobel Function: " << duration <<'\n';

		printf("FINISH\n");
		return 0;


}//main()


int gaussian(BYTE input_image[IMG_H][IMG_W], BYTE output_image[IMG_H][IMG_W])
{

/* Smoothing the input image before calling the Sobel filter (to improve accuracy)
This function is not included in the hardware
*/

	int gaus;

	IMG_H:for (int i=1; i<IMG_H-2; i++)
		{
		IMG_W:for (int j=1; j<IMG_W-2; j++)
			{

			//Gaussian


			gaus =
					input_image[i-1][j-1] / 16 + input_image[i][j-1] / 8 + input_image[i+1][j-1] / 16 +
					input_image[i-1][j] / 8 + input_image[i][j] / 4 + input_image[i+1][j] / 8 +
					input_image[i-1][j+1] / 16 + input_image[i][j+1] / 8 + input_image[i+1][j+1] / 16;

					output_image[i][j] = gaus;

			}
				}

}//gaussian



int opencv_sobel(BYTE input_image[IMG_H][IMG_W])
{



	cv::Mat src = cv::Mat(IMG_H, IMG_W, CV_8UC1, input_image);
	cv::Mat dst = src.clone();

	//for comparing performance
	clock_t start;
	double duration;
	start = std::clock();

	cv::Sobel(src, dst, -1, 1, 0);

	duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	std::cout<<"OpenCV Sobel Function: " << duration <<'\n';

	imwrite("lena_gray_sobel.jpg", dst);


}//opencv_sobel




