#include <hls_video.h>
#include <math.h>
#include <hls_stream.h>
using namespace hls;


typedef stream<ap_axiu<32,1,1,1> > AXI_STREAM;
typedef Scalar<3, unsigned char> RGB_PIXEL;
const int MAX_HEIGHT =512;
const int MAX_WIDTH =512;
typedef Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3> RGB_IMAGE;

//void image_filter(AXI_STREAM& video_in, AXI_STREAM& video_out, int rows, int cols);
void image_filter(AXI_STREAM& video_in, AXI_STREAM& video_out);
