#ifndef SOBLE_H_

#define SOBLE_H_
#include "hls_stream.h"

typedef unsigned char BYTE;
const int IMG_W = 512;
const int IMG_H = 512;
const int IMAGE_SIZE = IMG_W * IMG_H;

void sobel(BYTE g_cBufferOrg[IMG_H][IMG_W],BYTE g_cBufferDraw[IMG_H][IMG_W]);
//void sobel(hls::stream<BYTE>& input_image, hls::stream<BYTE>& output_image);
int gaussian(BYTE g_cBufferOrg[IMG_H][IMG_W],
				BYTE g_cBufferDraw[IMG_H][IMG_W]);

int opencv_sobel(BYTE g_cBufferOrg[IMG_H][IMG_W]);

#endif
