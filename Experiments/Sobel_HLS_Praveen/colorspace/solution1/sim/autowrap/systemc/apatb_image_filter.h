// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// ==============================================================

extern void image_filter (
hls::stream<struct ap_axiu<32, 1, 1, 1 > >& video_in,
hls::stream<struct ap_axiu<32, 1, 1, 1 > >& video_out);
