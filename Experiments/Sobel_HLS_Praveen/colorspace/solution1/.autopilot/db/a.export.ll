; ModuleID = 'C:/Users/praveen/Documents/MAS/colorspace/Vivado_project/colorspace/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@img_1_OC_data_stream_LF_2_NF_s = internal unnamed_addr constant [23 x i8] c"img_1.data_stream[2].V\00"
@img_1_OC_data_stream_LF_1_NF_s = internal unnamed_addr constant [23 x i8] c"img_1.data_stream[1].V\00"
@img_1_OC_data_stream_LF_0_NF_s = internal unnamed_addr constant [23 x i8] c"img_1.data_stream[0].V\00"
@img_0_OC_rows_OC_V_channel6_st = internal unnamed_addr constant [22 x i8] c"img_0.rows.V_channel6\00"
@img_0_OC_rows_OC_V_channel_str = internal unnamed_addr constant [21 x i8] c"img_0.rows.V_channel\00"
@img_0_OC_data_stream_LF_2_NF_s = internal unnamed_addr constant [23 x i8] c"img_0.data_stream[2].V\00"
@img_0_OC_data_stream_LF_1_NF_s = internal unnamed_addr constant [23 x i8] c"img_0.data_stream[1].V\00"
@img_0_OC_data_stream_LF_0_NF_s = internal unnamed_addr constant [23 x i8] c"img_0.data_stream[0].V\00"
@img_0_OC_cols_OC_V_channel7_st = internal unnamed_addr constant [22 x i8] c"img_0.cols.V_channel7\00"
@img_0_OC_cols_OC_V_channel_str = internal unnamed_addr constant [21 x i8] c"img_0.cols.V_channel\00"
@image_filter_str = internal unnamed_addr constant [13 x i8] c"image_filter\00"
@hls_KD_KD_LineBuffer_MD_6_MC_s = internal unnamed_addr constant [63 x i8] c"hls::LineBuffer<6, 512, unsigned char, 0>::LineBuffer.1.region\00"
@ap_fifo_str = internal unnamed_addr constant [8 x i8] c"ap_fifo\00"
@p_str1848 = private unnamed_addr constant [13 x i8] c"hls_label_17\00", align 1
@p_str1847 = private unnamed_addr constant [18 x i8] c"loop_wait_for_eol\00", align 1
@p_str1846 = private unnamed_addr constant [20 x i8] c"loop_wait_for_start\00", align 1
@p_str1819 = private unnamed_addr constant [13 x i8] c"hls_label_22\00", align 1
@p_str1815 = private unnamed_addr constant [11 x i8] c"loop_width\00", align 1
@p_str1814 = private unnamed_addr constant [12 x i8] c"loop_height\00", align 1
@p_str1808 = private unnamed_addr constant [14 x i8] c"OUTPUT_STREAM\00", align 1
@p_str1807 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str1806 = private unnamed_addr constant [13 x i8] c"INPUT_STREAM\00", align 1
@p_str1805 = private unnamed_addr constant [5 x i8] c"axis\00", align 1
@p_str = internal unnamed_addr constant [1 x i8] zeroinitializer

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare i12 @llvm.part.select.i12(i12, i32, i32) nounwind readnone

declare i11 @llvm.part.select.i11(i11, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define void @image_filter(i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i1* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i1* %INPUT_STREAM_V_id_V, i1* %INPUT_STREAM_V_dest_V, i32* %OUTPUT_STREAM_V_data_V, i4* %OUTPUT_STREAM_V_keep_V, i4* %OUTPUT_STREAM_V_strb_V, i1* %OUTPUT_STREAM_V_user_V, i1* %OUTPUT_STREAM_V_last_V, i1* %OUTPUT_STREAM_V_id_V, i1* %OUTPUT_STREAM_V_dest_V) {
codeRepl:
  %img_0_cols_V_channel7 = alloca i11, align 2
  %img_0_rows_V_channel6 = alloca i11, align 2
  %img_0_cols_V_channel = alloca i11, align 2
  %img_0_rows_V_channel = alloca i11, align 2
  call void (...)* @_ssdm_op_SpecDataflowPipeline(i32 -1, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %INPUT_STREAM_V_data_V), !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %INPUT_STREAM_V_keep_V), !map !11
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %INPUT_STREAM_V_strb_V), !map !15
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %INPUT_STREAM_V_user_V), !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %INPUT_STREAM_V_last_V), !map !23
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %INPUT_STREAM_V_id_V), !map !27
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %INPUT_STREAM_V_dest_V), !map !31
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %OUTPUT_STREAM_V_data_V), !map !35
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %OUTPUT_STREAM_V_keep_V), !map !39
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %OUTPUT_STREAM_V_strb_V), !map !43
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %OUTPUT_STREAM_V_user_V), !map !47
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %OUTPUT_STREAM_V_last_V), !map !51
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %OUTPUT_STREAM_V_id_V), !map !55
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %OUTPUT_STREAM_V_dest_V), !map !59
  call void (...)* @_ssdm_op_SpecTopModule([13 x i8]* @image_filter_str) nounwind
  %img_0_data_stream_0_V = alloca i8, align 1
  %empty = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_0_OC_data_stream_LF_0_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_0_data_stream_0_V, i8* %img_0_data_stream_0_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_0_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_0_data_stream_1_V = alloca i8, align 1
  %empty_11 = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_0_OC_data_stream_LF_1_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_0_data_stream_1_V, i8* %img_0_data_stream_1_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_0_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_0_data_stream_2_V = alloca i8, align 1
  %empty_12 = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_0_OC_data_stream_LF_2_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_0_data_stream_2_V, i8* %img_0_data_stream_2_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_0_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_1_data_stream_0_V = alloca i8, align 1
  %empty_13 = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_1_OC_data_stream_LF_0_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_1_data_stream_0_V, i8* %img_1_data_stream_0_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_1_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_1_data_stream_1_V = alloca i8, align 1
  %empty_14 = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_1_OC_data_stream_LF_1_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_1_data_stream_1_V, i8* %img_1_data_stream_1_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_1_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_1_data_stream_2_V = alloca i8, align 1
  %empty_15 = call i32 (...)* @_ssdm_op_SpecChannel([23 x i8]* @img_1_OC_data_stream_LF_2_NF_s, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 1, i8* %img_1_data_stream_2_V, i8* %img_1_data_stream_2_V)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_1_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i1* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i1* %INPUT_STREAM_V_id_V, i1* %INPUT_STREAM_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %OUTPUT_STREAM_V_data_V, i4* %OUTPUT_STREAM_V_keep_V, i4* %OUTPUT_STREAM_V_strb_V, i1* %OUTPUT_STREAM_V_user_V, i1* %OUTPUT_STREAM_V_last_V, i1* %OUTPUT_STREAM_V_id_V, i1* %OUTPUT_STREAM_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [14 x i8]* @p_str1808, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  %empty_16 = call i32 (...)* @_ssdm_op_SpecChannel([21 x i8]* @img_0_OC_rows_OC_V_channel_str, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 0, i11* %img_0_rows_V_channel, i11* %img_0_rows_V_channel)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_rows_V_channel, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %empty_17 = call i32 (...)* @_ssdm_op_SpecChannel([21 x i8]* @img_0_OC_cols_OC_V_channel_str, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 0, i11* %img_0_cols_V_channel, i11* %img_0_cols_V_channel)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_cols_V_channel, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call fastcc void @image_filter_Block__proc(i11* %img_0_rows_V_channel, i11* %img_0_cols_V_channel)
  %empty_18 = call i32 (...)* @_ssdm_op_SpecChannel([22 x i8]* @img_0_OC_rows_OC_V_channel6_st, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 0, i11* %img_0_rows_V_channel6, i11* %img_0_rows_V_channel6)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_rows_V_channel6, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %empty_19 = call i32 (...)* @_ssdm_op_SpecChannel([22 x i8]* @img_0_OC_cols_OC_V_channel7_st, i32 1, [1 x i8]* @p_str, [1 x i8]* @p_str, i32 1, i32 0, i11* %img_0_cols_V_channel7, i11* %img_0_cols_V_channel7)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_cols_V_channel7, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call fastcc void @"image_filter_AXIvideo2Mat<32, 512, 512, 32>"(i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i1* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i1* %INPUT_STREAM_V_id_V, i1* %INPUT_STREAM_V_dest_V, i11* nocapture %img_0_rows_V_channel, i11* nocapture %img_0_cols_V_channel, i8* %img_0_data_stream_0_V, i8* %img_0_data_stream_1_V, i8* %img_0_data_stream_2_V, i11* %img_0_rows_V_channel6, i11* %img_0_cols_V_channel7)
  call fastcc void @image_filter_Sobel(i11* nocapture %img_0_rows_V_channel6, i11* nocapture %img_0_cols_V_channel7, i8* %img_0_data_stream_0_V, i8* %img_0_data_stream_1_V, i8* %img_0_data_stream_2_V, i8* %img_1_data_stream_0_V, i8* %img_1_data_stream_1_V, i8* %img_1_data_stream_2_V)
  call fastcc void @"image_filter_Mat2AXIvideo<32, 512, 512, 32>"(i8* %img_1_data_stream_0_V, i8* %img_1_data_stream_1_V, i8* %img_1_data_stream_2_V, i32* %OUTPUT_STREAM_V_data_V, i4* %OUTPUT_STREAM_V_keep_V, i4* %OUTPUT_STREAM_V_strb_V, i1* %OUTPUT_STREAM_V_user_V, i1* %OUTPUT_STREAM_V_last_V, i1* %OUTPUT_STREAM_V_id_V, i1* %OUTPUT_STREAM_V_dest_V)
  ret void
}

define weak void @_ssdm_op_Write.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32*, i4*, i4*, i1*, i1*, i1*, i1*, i32, i4, i4, i1, i1, i1, i1) {
entry:
  store i32 %7, i32* %0
  store i4 %8, i4* %1
  store i4 %9, i4* %2
  store i1 %10, i1* %3
  store i1 %11, i1* %4
  store i1 %12, i1* %5
  store i1 %13, i1* %6
  ret void
}

define weak void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8*, i8) {
entry:
  %empty = call i8 @_autotb_FifoWrite_i8(i8* %0, i8 %1)
  ret void
}

define weak void @_ssdm_op_Write.ap_fifo.i11P(i11*, i11) {
entry:
  %empty = call i11 @_autotb_FifoWrite_i11(i11* %0, i11 %1)
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecProtocol(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecLoopTripCount(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecDataflowPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecChannel(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak { i32, i4, i4, i1, i1, i1, i1 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32*, i4*, i4*, i1*, i1*, i1*, i1*) {
entry:
  %empty = load i32* %0
  %empty_20 = load i4* %1
  %empty_21 = load i4* %2
  %empty_22 = load i1* %3
  %empty_23 = load i1* %4
  %empty_24 = load i1* %5
  %empty_25 = load i1* %6
  %mrv_0 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } undef, i32 %empty, 0
  %mrv1 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv_0, i4 %empty_20, 1
  %mrv2 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv1, i4 %empty_21, 2
  %mrv3 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv2, i1 %empty_22, 3
  %mrv4 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv3, i1 %empty_23, 4
  %mrv5 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv4, i1 %empty_24, 5
  %mrv6 = insertvalue { i32, i4, i4, i1, i1, i1, i1 } %mrv5, i1 %empty_25, 6
  ret { i32, i4, i4, i1, i1, i1, i1 } %mrv6
}

define weak i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8*) {
entry:
  %empty = call i8 @_autotb_FifoRead_i8(i8* %0)
  ret i8 %empty
}

define weak i11 @_ssdm_op_Read.ap_fifo.i11P(i11*) {
entry:
  %empty = call i11 @_autotb_FifoRead_i11(i11* %0)
  ret i11 %empty
}

define weak i11 @_ssdm_op_Read.ap_auto.i11(i11) {
entry:
  ret i11 %0
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_26 = trunc i32 %empty to i8
  ret i8 %empty_26
}

declare i8 @_ssdm_op_PartSelect.i8.i11.i32.i32(i11, i32, i32) nounwind readnone

define weak i3 @_ssdm_op_PartSelect.i3.i11.i32.i32(i11, i32, i32) nounwind readnone {
entry:
  %empty = call i11 @llvm.part.select.i11(i11 %0, i32 %1, i32 %2)
  %empty_27 = trunc i11 %empty to i3
  ret i3 %empty_27
}

declare i2 @_ssdm_op_PartSelect.i2.i14.i32.i32(i14, i32, i32) nounwind readnone

declare i2 @_ssdm_op_PartSelect.i2.i13.i32.i32(i13, i32, i32) nounwind readnone

declare i2 @_ssdm_op_PartSelect.i2.i11.i32.i32(i11, i32, i32) nounwind readnone

define weak i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12, i32, i32) nounwind readnone {
entry:
  %empty = call i12 @llvm.part.select.i12(i12 %0, i32 %1, i32 %2)
  %empty_28 = trunc i12 %empty to i11
  ret i11 %empty_28
}

define weak i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8, i8, i8, i2) {
entry:
  switch i2 %3, label %case2 [
    i2 0, label %case0
    i2 1, label %case1
  ]

case0:                                            ; preds = %case2, %case1, %entry
  %merge = phi i8 [ %0, %entry ], [ %1, %case1 ], [ %2, %case2 ]
  ret i8 %merge

case1:                                            ; preds = %entry
  br label %case0

case2:                                            ; preds = %entry
  br label %case0
}

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i13.i32(i13, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i13
  %empty_29 = shl i13 1, %empty
  %empty_30 = and i13 %0, %empty_29
  %empty_31 = icmp ne i13 %empty_30, 0
  ret i1 %empty_31
}

define weak i1 @_ssdm_op_BitSelect.i1.i11.i32(i11, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i11
  %empty_32 = shl i11 1, %empty
  %empty_33 = and i11 %0, %empty_32
  %empty_34 = icmp ne i11 %empty_33, 0
  ret i1 %empty_34
}

define weak i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8, i1) nounwind readnone {
entry:
  %empty = zext i8 %0 to i9
  %empty_35 = zext i1 %1 to i9
  %empty_36 = shl i9 %empty, 1
  %empty_37 = or i9 %empty_36, %empty_35
  ret i9 %empty_37
}

define weak i32 @_ssdm_op_BitConcatenate.i32.i8.i8.i8.i8(i8, i8, i8, i8) nounwind readnone {
entry:
  %empty = zext i8 %2 to i16
  %empty_38 = zext i8 %3 to i16
  %empty_39 = shl i16 %empty, 8
  %empty_40 = or i16 %empty_39, %empty_38
  %empty_41 = zext i8 %1 to i24
  %empty_42 = zext i16 %empty_40 to i24
  %empty_43 = shl i24 %empty_41, 16
  %empty_44 = or i24 %empty_43, %empty_42
  %empty_45 = zext i8 %0 to i32
  %empty_46 = zext i24 %empty_44 to i32
  %empty_47 = shl i32 %empty_45, 24
  %empty_48 = or i32 %empty_47, %empty_46
  ret i32 %empty_48
}

define weak i12 @_ssdm_op_BitConcatenate.i12.i11.i1(i11, i1) nounwind readnone {
entry:
  %empty = zext i11 %0 to i12
  %empty_49 = zext i1 %1 to i12
  %empty_50 = shl i12 %empty, 1
  %empty_51 = or i12 %empty_50, %empty_49
  ret i12 %empty_51
}

declare void @_ssdm_SpecDependence(...) nounwind

declare i8 @_autotb_FifoWrite_i8(i8*, i8)

declare i11 @_autotb_FifoWrite_i11(i11*, i11)

declare i8 @_autotb_FifoRead_i8(i8*)

declare i11 @_autotb_FifoRead_i11(i11*)

declare void @_GLOBAL__I_a() nounwind

define internal fastcc void @image_filter_Sobel(i11* nocapture %p_src_rows_V, i11* nocapture %p_src_cols_V, i8* %p_src_data_stream_0_V, i8* %p_src_data_stream_1_V, i8* %p_src_data_stream_2_V, i8* %p_dst_data_stream_0_V, i8* %p_dst_data_stream_1_V, i8* %p_dst_data_stream_2_V) {
entry:
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i11* %p_src_rows_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i11* %p_src_cols_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %p_src_rows_V_read = call i11 @_ssdm_op_Read.ap_fifo.i11P(i11* %p_src_rows_V)
  %p_src_cols_V_read = call i11 @_ssdm_op_Read.ap_fifo.i11P(i11* %p_src_cols_V)
  call fastcc void @image_filter_Filter2D(i11 %p_src_rows_V_read, i11 %p_src_cols_V_read, i8* %p_src_data_stream_0_V, i8* %p_src_data_stream_1_V, i8* %p_src_data_stream_2_V, i8* %p_dst_data_stream_0_V, i8* %p_dst_data_stream_1_V, i8* %p_dst_data_stream_2_V)
  ret void
}

define internal fastcc void @"image_filter_Mat2AXIvideo<32, 512, 512, 32>"(i8* %img_data_stream_0_V, i8* %img_data_stream_1_V, i8* %img_data_stream_2_V, i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V) {
._crit_edge:
  %tmp_user_V = alloca i1
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [14 x i8]* @p_str1808, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  store i1 true, i1* %tmp_user_V
  br label %0

; <label>:0                                       ; preds = %3, %._crit_edge
  %p_s = phi i10 [ 0, %._crit_edge ], [ %i_V, %3 ]
  %exitcond4 = icmp eq i10 %p_s, -512
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 512, i64 512, i64 512)
  %i_V = add i10 %p_s, 1
  br i1 %exitcond4, label %4, label %1

; <label>:1                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([12 x i8]* @p_str1814) nounwind
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str1814)
  br label %2

; <label>:2                                       ; preds = %"operator>>.exit", %1
  %p_2 = phi i10 [ 0, %1 ], [ %j_V, %"operator>>.exit" ]
  %exitcond5 = icmp eq i10 %p_2, -512
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 512, i64 512, i64 512)
  %j_V = add i10 %p_2, 1
  br i1 %exitcond5, label %3, label %"operator>>.exit"

"operator>>.exit":                                ; preds = %2
  %tmp_user_V_load = load i1* %tmp_user_V
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str1815) nounwind
  %tmp_2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @p_str1815)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1807) nounwind
  %axi_last_V = icmp eq i10 %p_2, 511
  %tmp_3 = call i32 (...)* @_ssdm_op_SpecRegionBegin([13 x i8]* @p_str1819)
  call void (...)* @_ssdm_op_SpecProtocol(i32 0, [1 x i8]* @p_str1807) nounwind
  %tmp_6 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %img_data_stream_0_V)
  %tmp_7 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %img_data_stream_1_V)
  %tmp_5 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %img_data_stream_2_V)
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([13 x i8]* @p_str1819, i32 %tmp_3)
  %tmp_data_V = call i32 @_ssdm_op_BitConcatenate.i32.i8.i8.i8.i8(i8 -1, i8 %tmp_5, i8 %tmp_7, i8 %tmp_6)
  call void @_ssdm_op_Write.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, i32 %tmp_data_V, i4 -1, i4 undef, i1 %tmp_user_V_load, i1 %axi_last_V, i1 undef, i1 undef)
  %empty_52 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @p_str1815, i32 %tmp_2)
  store i1 false, i1* %tmp_user_V
  br label %2

; <label>:3                                       ; preds = %2
  %empty_53 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str1814, i32 %tmp)
  br label %0

; <label>:4                                       ; preds = %0
  ret void
}

define internal fastcc void @image_filter_Filter2D(i11 %p_src_rows_V_read, i11 %p_src_cols_V_read, i8* %p_src_data_stream_0_V, i8* %p_src_data_stream_1_V, i8* %p_src_data_stream_2_V, i8* %p_dst_data_stream_0_V, i8* %p_dst_data_stream_1_V, i8* %p_dst_data_stream_2_V) {
arrayctor.loop1.i.preheader:
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_src_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %p_dst_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %p_src_cols_V_read_1 = call i11 @_ssdm_op_Read.ap_auto.i11(i11 %p_src_cols_V_read)
  %p_src_rows_V_read_1 = call i11 @_ssdm_op_Read.ap_auto.i11(i11 %p_src_rows_V_read)
  %p_src_cols_V_read_cast = sext i11 %p_src_cols_V_read_1 to i12
  %p_src_rows_V_read_cast = sext i11 %p_src_rows_V_read_1 to i12
  %k_buf_0_val_3 = alloca [512 x i8], align 1
  %k_buf_0_val_4 = alloca [512 x i8], align 1
  %k_buf_0_val_5 = alloca [512 x i8], align 1
  %k_buf_1_val_3 = alloca [512 x i8], align 1
  %k_buf_1_val_4 = alloca [512 x i8], align 1
  %k_buf_1_val_5 = alloca [512 x i8], align 1
  %k_buf_2_val_3 = alloca [512 x i8], align 1
  %k_buf_2_val_4 = alloca [512 x i8], align 1
  %k_buf_2_val_5 = alloca [512 x i8], align 1
  %rows_cast = zext i12 %p_src_rows_V_read_cast to i13
  %cols_cast = zext i12 %p_src_cols_V_read_cast to i13
  br label %arrayctor.loop1.i

arrayctor.loop1.i:                                ; preds = %arrayctor.loop1.i354_ifconv, %arrayctor.loop1.i.preheader
  %tmp_5 = phi i2 [ %tmp_6, %arrayctor.loop1.i354_ifconv ], [ 0, %arrayctor.loop1.i.preheader ]
  %tmp_6 = add i2 %tmp_5, 1
  %rbegin_i_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([63 x i8]* @hls_KD_KD_LineBuffer_MD_6_MC_s) nounwind
  switch i2 %tmp_5, label %branch2 [
    i2 0, label %branch0
    i2 1, label %branch1
  ]

arrayctor.loop1.i354_ifconv:                      ; preds = %branch2, %branch1, %branch0
  %rend_i_i = call i32 (...)* @_ssdm_op_SpecRegionEnd([63 x i8]* @hls_KD_KD_LineBuffer_MD_6_MC_s, i32 %rbegin_i_i) nounwind
  %tmp_7 = icmp eq i2 %tmp_5, -2
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 3, i64 3, i64 3)
  br i1 %tmp_7, label %._crit_edge.i, label %arrayctor.loop1.i

._crit_edge.i:                                    ; preds = %arrayctor.loop1.i354_ifconv
  %src_kernel_win_0_val_0_1 = alloca i8
  %src_kernel_win_0_val_0_1_1 = alloca i8
  %src_kernel_win_0_val_1_1 = alloca i8
  %src_kernel_win_0_val_1_1_1 = alloca i8
  %src_kernel_win_0_val_2_1 = alloca i8
  %src_kernel_win_0_val_2_1_1 = alloca i8
  %src_kernel_win_1_val_0_1 = alloca i8
  %src_kernel_win_1_val_0_1_1 = alloca i8
  %src_kernel_win_1_val_1_1 = alloca i8
  %src_kernel_win_1_val_1_1_1 = alloca i8
  %src_kernel_win_1_val_2_1 = alloca i8
  %src_kernel_win_1_val_2_1_1 = alloca i8
  %src_kernel_win_2_val_0_1 = alloca i8
  %src_kernel_win_2_val_0_1_1 = alloca i8
  %src_kernel_win_2_val_1_1 = alloca i8
  %src_kernel_win_2_val_1_1_1 = alloca i8
  %src_kernel_win_2_val_2_1 = alloca i8
  %src_kernel_win_2_val_2_1_1 = alloca i8
  %right_border_buf_0_val_0_1 = alloca i8
  %right_border_buf_0_val_0_1_1 = alloca i8
  %right_border_buf_2_val_2_1 = alloca i8
  %right_border_buf_0_val_1_1 = alloca i8
  %right_border_buf_0_val_1_1_1 = alloca i8
  %right_border_buf_2_val_2_1_1 = alloca i8
  %right_border_buf_0_val_2_1 = alloca i8
  %right_border_buf_0_val_2_1_1 = alloca i8
  %right_border_buf_2_val_1_1 = alloca i8
  %right_border_buf_1_val_0_1 = alloca i8
  %right_border_buf_1_val_0_1_1 = alloca i8
  %right_border_buf_2_val_1_1_1 = alloca i8
  %right_border_buf_1_val_1_1 = alloca i8
  %right_border_buf_1_val_1_1_1 = alloca i8
  %right_border_buf_2_val_0_1 = alloca i8
  %right_border_buf_1_val_2_1 = alloca i8
  %right_border_buf_1_val_2_1_1 = alloca i8
  %right_border_buf_2_val_0_1_1 = alloca i8
  %heightloop = add i13 2, %rows_cast
  %widthloop = add i13 2, %cols_cast
  %tmp = trunc i11 %p_src_rows_V_read_1 to i2
  %p_neg392_i_cast = add i2 -1, %tmp
  %tmp_8 = trunc i11 %p_src_cols_V_read_1 to i2
  %not_tmp_s = icmp ne i11 %p_src_rows_V_read_1, 1
  %p_anchor_2_1_cast = zext i1 %not_tmp_s to i13
  %tmp_s = icmp eq i11 %p_src_rows_V_read_1, 1
  %tmp_2 = call i12 @_ssdm_op_BitConcatenate.i12.i11.i1(i11 %p_src_rows_V_read_1, i1 false)
  %tmp_1 = sext i12 %tmp_2 to i13
  %tmp_154_0_cast = zext i13 %tmp_1 to i14
  %tmp_9 = add i14 2, %tmp_154_0_cast
  %tmp_61_2 = icmp eq i11 %p_src_cols_V_read_1, 1
  %tmp_3 = call i12 @_ssdm_op_BitConcatenate.i12.i11.i1(i11 %p_src_cols_V_read_1, i1 false)
  %tmp_64_2 = sext i12 %tmp_3 to i13
  %tmp_64_2_cast = zext i13 %tmp_64_2 to i14
  %tmp_65_2 = add i14 -2, %tmp_64_2_cast
  %tmp_4 = add i2 -1, %tmp_8
  br label %0

; <label>:0                                       ; preds = %9, %._crit_edge.i
  %p_014_0_i = phi i12 [ 0, %._crit_edge.i ], [ %i_V, %9 ]
  %tmp_14_cast = zext i12 %p_014_0_i to i13
  %tmp_10 = icmp ult i13 %tmp_14_cast, %heightloop
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 9, i64 515, i64 0)
  %i_V = add i12 %p_014_0_i, 1
  br i1 %tmp_10, label %1, label %"filter<32, 32, ap_int<8>, int, 512, 512, 3, 3>.exit"

; <label>:1                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([12 x i8]* @p_str1814) nounwind
  %tmp_11 = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str1814)
  %tmp_12 = icmp ult i12 %p_014_0_i, %p_src_rows_V_read_cast
  %ult = icmp ult i12 %p_014_0_i, %p_src_rows_V_read_cast
  %rev = xor i1 %ult, true
  %tmp_13 = call i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12 %p_014_0_i, i32 1, i32 11)
  %icmp = icmp ne i11 %tmp_13, 0
  %tmp_14 = icmp eq i13 %p_anchor_2_1_cast, %tmp_14_cast
  %tmp_116_0_1 = icmp eq i12 %p_014_0_i, 0
  %tmp_116_0_2 = icmp eq i12 %p_014_0_i, 1
  %tmp_17 = icmp ugt i12 %p_014_0_i, %p_src_rows_V_read_cast
  %tmp_18 = add i13 -1, %tmp_14_cast
  %tmp_20 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %tmp_18, i32 12)
  %rev1 = xor i1 %tmp_20, true
  %tmp_22 = icmp slt i13 %tmp_18, %rows_cast
  %or_cond_i423_i = and i1 %tmp_22, %rev1
  %tmp_23 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %tmp_18, i32 12)
  %p_assign_7 = sub i13 1, %tmp_14_cast
  %p_p2_i424_i = select i1 %tmp_23, i13 %p_assign_7, i13 %tmp_18
  %tmp_24 = icmp slt i13 %p_p2_i424_i, %rows_cast
  %tmp_25 = trunc i14 %tmp_9 to i2
  %tmp_26 = trunc i13 %p_p2_i424_i to i2
  %p_assign_6_0_1 = add i13 -2, %tmp_14_cast
  %tmp_27 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_assign_6_0_1, i32 12)
  %rev2 = xor i1 %tmp_27, true
  %tmp_142_0_1 = icmp slt i13 %p_assign_6_0_1, %rows_cast
  %or_cond_i423_i_0_1 = and i1 %tmp_142_0_1, %rev2
  %tmp_28 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_assign_6_0_1, i32 12)
  %p_assign_7_0_1 = sub i13 2, %tmp_14_cast
  %p_p2_i424_i_0_1 = select i1 %tmp_28, i13 %p_assign_7_0_1, i13 %p_assign_6_0_1
  %tmp_152_0_1 = icmp slt i13 %p_p2_i424_i_0_1, %rows_cast
  %tmp_29 = trunc i14 %tmp_9 to i2
  %tmp_34 = trunc i13 %p_p2_i424_i_0_1 to i2
  %p_assign_6_0_2 = add i13 -3, %tmp_14_cast
  %tmp_36 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_assign_6_0_2, i32 12)
  %rev3 = xor i1 %tmp_36, true
  %tmp_142_0_2 = icmp slt i13 %p_assign_6_0_2, %rows_cast
  %or_cond_i423_i_0_2 = and i1 %tmp_142_0_2, %rev3
  %tmp_39 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_assign_6_0_2, i32 12)
  %p_assign_7_0_2 = sub i13 3, %tmp_14_cast
  %p_p2_i424_i_0_2 = select i1 %tmp_39, i13 %p_assign_7_0_2, i13 %p_assign_6_0_2
  %tmp_152_0_2 = icmp slt i13 %p_p2_i424_i_0_2, %rows_cast
  %tmp_41 = trunc i14 %tmp_9 to i2
  %tmp_44 = trunc i13 %p_p2_i424_i_0_2 to i2
  %brmerge = or i1 %or_cond_i423_i, %tmp_s
  %tmp_46 = trunc i13 %tmp_18 to i2
  %tmp_35 = select i1 %or_cond_i423_i, i2 %tmp_46, i2 0
  %tmp_49 = trunc i13 %p_p2_i424_i to i2
  %tmp_51 = sub i2 %tmp_25, %tmp_26
  %tmp_50 = select i1 %tmp_24, i2 %tmp_49, i2 %tmp_51
  %tmp_37 = select i1 %brmerge, i2 %tmp_35, i2 %tmp_50
  %row_assign_9 = sub i2 %p_neg392_i_cast, %tmp_37
  %brmerge1 = or i1 %or_cond_i423_i_0_1, %tmp_s
  %tmp_53 = trunc i13 %p_assign_6_0_1 to i2
  %tmp_40 = select i1 %or_cond_i423_i_0_1, i2 %tmp_53, i2 0
  %tmp_61 = trunc i13 %p_p2_i424_i_0_1 to i2
  %tmp_62 = sub i2 %tmp_29, %tmp_34
  %tmp_60 = select i1 %tmp_152_0_1, i2 %tmp_61, i2 %tmp_62
  %tmp_42 = select i1 %brmerge1, i2 %tmp_40, i2 %tmp_60
  %row_assign_9_0_1_t = sub i2 %p_neg392_i_cast, %tmp_42
  %brmerge2 = or i1 %or_cond_i423_i_0_2, %tmp_s
  %tmp_71 = trunc i13 %p_assign_6_0_2 to i2
  %tmp_45 = select i1 %or_cond_i423_i_0_2, i2 %tmp_71, i2 0
  %tmp_72 = trunc i13 %p_p2_i424_i_0_2 to i2
  %tmp_74 = sub i2 %tmp_41, %tmp_44
  %tmp_64 = select i1 %tmp_152_0_2, i2 %tmp_72, i2 %tmp_74
  %tmp_47 = select i1 %brmerge2, i2 %tmp_45, i2 %tmp_64
  %row_assign_9_0_2_t = sub i2 %p_neg392_i_cast, %tmp_47
  br label %2

; <label>:2                                       ; preds = %._crit_edge412.i.2, %1
  %p_027_0_i = phi i12 [ 0, %1 ], [ %j_V, %._crit_edge412.i.2 ]
  %right_border_buf_2_val_2_1_2 = load i8* %right_border_buf_2_val_2_1_1
  %right_border_buf_2_val_1_1_2 = load i8* %right_border_buf_2_val_1_1_1
  %right_border_buf_2_val_0_1_2 = load i8* %right_border_buf_2_val_0_1_1
  %tmp_18_cast = zext i12 %p_027_0_i to i13
  %tmp_15 = icmp ult i13 %tmp_18_cast, %widthloop
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 514, i64 0)
  %j_V = add i12 %p_027_0_i, 1
  br i1 %tmp_15, label %_ifconv, label %9

_ifconv:                                          ; preds = %2
  %right_border_buf_0_val_0_1_s = load i8* %right_border_buf_0_val_0_1
  %right_border_buf_0_val_0_1_1_54 = load i8* %right_border_buf_0_val_0_1_1
  %right_border_buf_0_val_1_1_s = load i8* %right_border_buf_0_val_1_1
  %right_border_buf_0_val_1_1_1_55 = load i8* %right_border_buf_0_val_1_1_1
  %right_border_buf_0_val_2_1_s = load i8* %right_border_buf_0_val_2_1
  %right_border_buf_0_val_2_1_1_56 = load i8* %right_border_buf_0_val_2_1_1
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str1815) nounwind
  %tmp_16 = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @p_str1815)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1807) nounwind
  %tmp_75 = call i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12 %p_027_0_i, i32 1, i32 11)
  %icmp1 = icmp ne i11 %tmp_75, 0
  %ImagLoc_x = add i13 -1, %tmp_18_cast
  %tmp_76 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %ImagLoc_x, i32 12)
  %rev4 = xor i1 %tmp_76, true
  %tmp_19 = icmp slt i13 %ImagLoc_x, %cols_cast
  %or_cond_i_i = and i1 %tmp_19, %rev4
  %brmerge3 = or i1 %or_cond_i_i, %tmp_61_2
  %ImagLoc_x_cast_mux = select i1 %or_cond_i_i, i13 %ImagLoc_x, i13 0
  %ImagLoc_x_cast_mux_cast = zext i13 %ImagLoc_x_cast_mux to i14
  %tmp_77 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %ImagLoc_x, i32 12)
  %p_assign_1 = sub i13 1, %tmp_18_cast
  %p_p2_i_i = select i1 %tmp_77, i13 %p_assign_1, i13 %ImagLoc_x
  %p_p2_i_i_cast = sext i13 %p_p2_i_i to i14
  %tmp_21 = icmp slt i13 %p_p2_i_i, %cols_cast
  %p_assign_2 = sub i14 %tmp_65_2, %p_p2_i_i_cast
  %sel_tmp = select i1 %brmerge3, i14 %ImagLoc_x_cast_mux_cast, i14 %p_assign_2
  %sel_tmp1 = xor i1 %brmerge3, true
  %sel_tmp2 = and i1 %tmp_21, %sel_tmp1
  %x = select i1 %sel_tmp2, i14 %p_p2_i_i_cast, i14 %sel_tmp
  %x_cast = sext i14 %x to i32
  %tmp_78 = trunc i14 %x to i2
  %brmerge8 = or i1 %rev, %tmp_19
  %tmp_30 = zext i32 %x_cast to i64
  %k_buf_0_val_3_addr = getelementptr [512 x i8]* %k_buf_0_val_3, i64 0, i64 %tmp_30
  %k_buf_0_val_3_load = load i8* %k_buf_0_val_3_addr, align 1
  %col_assign_2_0_t = sub i2 %tmp_4, %tmp_78
  %tmp_31 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_0_val_0_1_s, i8 %right_border_buf_0_val_0_1_1_54, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_0_val_0_0 = select i1 %brmerge8, i8 %k_buf_0_val_3_load, i8 %tmp_31
  %k_buf_0_val_4_addr = getelementptr [512 x i8]* %k_buf_0_val_4, i64 0, i64 %tmp_30
  %k_buf_0_val_4_load = load i8* %k_buf_0_val_4_addr, align 1
  %tmp_32 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_0_val_1_1_s, i8 %right_border_buf_0_val_1_1_1_55, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_0_val_1_0 = select i1 %brmerge8, i8 %k_buf_0_val_4_load, i8 %tmp_32
  %k_buf_0_val_5_addr = getelementptr [512 x i8]* %k_buf_0_val_5, i64 0, i64 %tmp_30
  %k_buf_0_val_5_load = load i8* %k_buf_0_val_5_addr, align 1
  %tmp_33 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_0_val_2_1_s, i8 %right_border_buf_0_val_2_1_1_56, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_0_val_2_0 = select i1 %brmerge8, i8 %k_buf_0_val_5_load, i8 %tmp_33
  br i1 %or_cond_i_i, label %4, label %._crit_edge405.i.0_ifconv

._crit_edge412.i.0_ifconv:                        ; preds = %.preheader.0, %._crit_edge405.i.0_ifconv
  %right_border_buf_1_val_0_1_s = load i8* %right_border_buf_1_val_0_1
  %right_border_buf_1_val_0_1_1_57 = load i8* %right_border_buf_1_val_0_1_1
  %right_border_buf_1_val_1_1_s = load i8* %right_border_buf_1_val_1_1
  %right_border_buf_1_val_1_1_1_58 = load i8* %right_border_buf_1_val_1_1_1
  %right_border_buf_1_val_2_1_s = load i8* %right_border_buf_1_val_2_1
  %right_border_buf_1_val_2_1_1_59 = load i8* %right_border_buf_1_val_2_1_1
  %k_buf_1_val_3_addr = getelementptr [512 x i8]* %k_buf_1_val_3, i64 0, i64 %tmp_30
  %k_buf_1_val_3_load = load i8* %k_buf_1_val_3_addr, align 1
  %tmp_54 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_1_val_0_1_s, i8 %right_border_buf_1_val_0_1_1_57, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_1_val_0_0 = select i1 %brmerge8, i8 %k_buf_1_val_3_load, i8 %tmp_54
  %k_buf_1_val_4_addr = getelementptr [512 x i8]* %k_buf_1_val_4, i64 0, i64 %tmp_30
  %k_buf_1_val_4_load = load i8* %k_buf_1_val_4_addr, align 1
  %tmp_55 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_1_val_1_1_s, i8 %right_border_buf_1_val_1_1_1_58, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_1_val_1_0 = select i1 %brmerge8, i8 %k_buf_1_val_4_load, i8 %tmp_55
  %k_buf_1_val_5_addr = getelementptr [512 x i8]* %k_buf_1_val_5, i64 0, i64 %tmp_30
  %k_buf_1_val_5_load = load i8* %k_buf_1_val_5_addr, align 1
  %tmp_56 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_1_val_2_1_s, i8 %right_border_buf_1_val_2_1_1_59, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_1_val_2_0 = select i1 %brmerge8, i8 %k_buf_1_val_5_load, i8 %tmp_56
  br i1 %or_cond_i_i, label %6, label %._crit_edge405.i.1_ifconv

._crit_edge405.i.0_ifconv:                        ; preds = %._crit_edge407.i.0.2, %.preheader390.i.preheader.0, %3, %_ifconv
  %tmp_38 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_0_val_0_0, i8 %col_buf_0_val_1_0, i8 %col_buf_0_val_2_0, i2 %row_assign_9)
  %src_kernel_win_0_val_0_0 = select i1 %tmp_17, i8 %tmp_38, i8 %col_buf_0_val_0_0
  %tmp_43 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_0_val_0_0, i8 %col_buf_0_val_1_0, i8 %col_buf_0_val_2_0, i2 %row_assign_9_0_1_t)
  %src_kernel_win_0_val_1_0 = select i1 %tmp_17, i8 %tmp_43, i8 %col_buf_0_val_1_0
  %tmp_48 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_0_val_0_0, i8 %col_buf_0_val_1_0, i8 %col_buf_0_val_2_0, i2 %row_assign_9_0_2_t)
  %src_kernel_win_0_val_2_0 = select i1 %tmp_17, i8 %tmp_48, i8 %col_buf_0_val_2_0
  %or_cond_i = and i1 %icmp, %icmp1
  br i1 %or_cond_i, label %.preheader.0, label %._crit_edge412.i.0_ifconv

; <label>:3                                       ; preds = %4
  br i1 %tmp_12, label %.preheader390.i.preheader.0, label %._crit_edge405.i.0_ifconv

; <label>:4                                       ; preds = %_ifconv
  br i1 %icmp, label %3, label %borderInterpolate.exit422.i.0.0

.preheader390.i.preheader.0:                      ; preds = %3
  %right_border_buf_0_val_0_1_2 = load i8* %right_border_buf_0_val_0_1
  %right_border_buf_0_val_1_1_2 = load i8* %right_border_buf_0_val_1_1
  %right_border_buf_0_val_2_1_2 = load i8* %right_border_buf_0_val_2_1
  store i8 %k_buf_0_val_4_load, i8* %k_buf_0_val_5_addr, align 1
  store i8 %k_buf_0_val_3_load, i8* %k_buf_0_val_4_addr, align 1
  %tmp_80 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_0_V)
  store i8 %tmp_80, i8* %k_buf_0_val_3_addr, align 1
  store i8 %right_border_buf_0_val_2_1_2, i8* %right_border_buf_0_val_2_1_1
  store i8 %col_buf_0_val_2_0, i8* %right_border_buf_0_val_2_1
  store i8 %right_border_buf_0_val_1_1_2, i8* %right_border_buf_0_val_1_1_1
  store i8 %col_buf_0_val_1_0, i8* %right_border_buf_0_val_1_1
  store i8 %right_border_buf_0_val_0_1_2, i8* %right_border_buf_0_val_0_1_1
  store i8 %col_buf_0_val_0_0, i8* %right_border_buf_0_val_0_1
  br label %._crit_edge405.i.0_ifconv

"operator().exit465.i.0.0":                       ; preds = %borderInterpolate.exit422.i.0.0
  store i8 %tmp_79, i8* %k_buf_0_val_5_addr, align 1
  br label %._crit_edge407.i.0.0

._crit_edge407.i.0.0:                             ; preds = %borderInterpolate.exit422.i.0.0, %"operator().exit465.i.0.0"
  br i1 %tmp_116_0_1, label %"operator().exit465.i.0.1", label %._crit_edge407.i.0.1

"operator().exit465.i.0.1":                       ; preds = %._crit_edge407.i.0.0
  store i8 %tmp_79, i8* %k_buf_0_val_4_addr, align 1
  br label %._crit_edge407.i.0.1

._crit_edge407.i.0.1:                             ; preds = %"operator().exit465.i.0.1", %._crit_edge407.i.0.0
  br i1 %tmp_116_0_2, label %"operator().exit465.i.0.2", label %._crit_edge407.i.0.2

"operator().exit465.i.0.2":                       ; preds = %._crit_edge407.i.0.1
  store i8 %tmp_79, i8* %k_buf_0_val_3_addr, align 1
  br label %._crit_edge407.i.0.2

._crit_edge407.i.0.2:                             ; preds = %"operator().exit465.i.0.2", %._crit_edge407.i.0.1
  br label %._crit_edge405.i.0_ifconv

borderInterpolate.exit422.i.0.0:                  ; preds = %4
  %tmp_79 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_0_V)
  br i1 %tmp_14, label %"operator().exit465.i.0.0", label %._crit_edge407.i.0.0

.preheader.0:                                     ; preds = %._crit_edge405.i.0_ifconv
  %src_kernel_win_0_val_0_1_1_s = load i8* %src_kernel_win_0_val_0_1_1
  %src_kernel_win_0_val_1_1_1_s = load i8* %src_kernel_win_0_val_1_1_1
  %src_kernel_win_0_val_2_1_1_s = load i8* %src_kernel_win_0_val_2_1_1
  %OP1_V_0_0_cast = zext i8 %src_kernel_win_0_val_2_1_1_s to i9
  %tmp_160_0_0_2_cast = zext i8 %src_kernel_win_0_val_2_0 to i9
  %p_Val2_5_0_0_2 = sub i9 %tmp_160_0_0_2_cast, %OP1_V_0_0_cast
  %p_Val2_5_0_0_2_cast_cast = sext i9 %p_Val2_5_0_0_2 to i10
  %p_shl = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_0_val_1_1_1_s, i1 false)
  %p_shl_cast = zext i9 %p_shl to i10
  %p_Val2_0_1 = sub i10 0, %p_shl_cast
  %tmp_160_0_1_cast = sext i10 %p_Val2_0_1 to i11
  %p_Val2_0_1_2 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_0_val_1_0, i1 false)
  %tmp_160_0_1_cast_60 = zext i9 %p_Val2_0_1_2 to i11
  %OP1_V_0_2_cast = zext i8 %src_kernel_win_0_val_0_1_1_s to i9
  %p_Val2_0_2 = sub i9 0, %OP1_V_0_2_cast
  %tmp_160_0_2_cast = sext i9 %p_Val2_0_2 to i11
  %tmp_160_0_2_2_cast_cast = zext i8 %src_kernel_win_0_val_0_0 to i10
  %tmp49 = add i11 %tmp_160_0_2_cast, %tmp_160_0_1_cast
  %tmp50 = add i10 %tmp_160_0_2_2_cast_cast, %p_Val2_5_0_0_2_cast_cast
  %tmp71_cast = sext i10 %tmp50 to i11
  %tmp51 = add i11 %tmp_160_0_1_cast_60, %tmp71_cast
  %p_Val2_1 = add i11 %tmp49, %tmp51
  %isneg = call i1 @_ssdm_op_BitSelect.i1.i11.i32(i11 %p_Val2_1, i32 10)
  %p_Val2_2 = trunc i11 %p_Val2_1 to i8
  %tmp_52 = call i3 @_ssdm_op_PartSelect.i3.i11.i32.i32(i11 %p_Val2_1, i32 8, i32 10)
  %tmp_2_i_i = xor i1 %isneg, true
  %not_i_i_i = icmp ne i3 %tmp_52, 0
  %overflow = and i1 %not_i_i_i, %tmp_2_i_i
  %p_mux_i_i_cast = select i1 %tmp_2_i_i, i8 -1, i8 0
  %tmp_i_i = or i1 %isneg, %overflow
  %p_Val2_9 = select i1 %tmp_i_i, i8 %p_mux_i_i_cast, i8 %p_Val2_2
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %p_dst_data_stream_0_V, i8 %p_Val2_9)
  br label %._crit_edge412.i.0_ifconv

._crit_edge412.i.1_ifconv:                        ; preds = %.preheader.1, %._crit_edge405.i.1_ifconv
  %right_border_buf_2_val_2_1_s = load i8* %right_border_buf_2_val_2_1
  %right_border_buf_2_val_1_1_s = load i8* %right_border_buf_2_val_1_1
  %right_border_buf_2_val_0_1_s = load i8* %right_border_buf_2_val_0_1
  %k_buf_2_val_3_addr = getelementptr [512 x i8]* %k_buf_2_val_3, i64 0, i64 %tmp_30
  %k_buf_2_val_3_load = load i8* %k_buf_2_val_3_addr, align 1
  %tmp_65 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_2_val_0_1_2, i8 %right_border_buf_2_val_0_1_s, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_2_val_0_0 = select i1 %brmerge8, i8 %k_buf_2_val_3_load, i8 %tmp_65
  %k_buf_2_val_4_addr = getelementptr [512 x i8]* %k_buf_2_val_4, i64 0, i64 %tmp_30
  %k_buf_2_val_4_load = load i8* %k_buf_2_val_4_addr, align 1
  %tmp_66 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_2_val_1_1_2, i8 %right_border_buf_2_val_1_1_s, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_2_val_1_0 = select i1 %brmerge8, i8 %k_buf_2_val_4_load, i8 %tmp_66
  %k_buf_2_val_5_addr = getelementptr [512 x i8]* %k_buf_2_val_5, i64 0, i64 %tmp_30
  %k_buf_2_val_5_load = load i8* %k_buf_2_val_5_addr, align 1
  %tmp_67 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %right_border_buf_2_val_2_1_2, i8 %right_border_buf_2_val_2_1_s, i8 undef, i2 %col_assign_2_0_t)
  %col_buf_2_val_2_0 = select i1 %brmerge8, i8 %k_buf_2_val_5_load, i8 %tmp_67
  br i1 %or_cond_i_i, label %8, label %._crit_edge405.i.2_ifconv

._crit_edge405.i.1_ifconv:                        ; preds = %._crit_edge407.i.1.2, %.preheader390.i.preheader.1, %5, %._crit_edge412.i.0_ifconv
  %tmp_57 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_1_val_0_0, i8 %col_buf_1_val_1_0, i8 %col_buf_1_val_2_0, i2 %row_assign_9)
  %src_kernel_win_1_val_0_0 = select i1 %tmp_17, i8 %tmp_57, i8 %col_buf_1_val_0_0
  %tmp_58 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_1_val_0_0, i8 %col_buf_1_val_1_0, i8 %col_buf_1_val_2_0, i2 %row_assign_9_0_1_t)
  %src_kernel_win_1_val_1_0 = select i1 %tmp_17, i8 %tmp_58, i8 %col_buf_1_val_1_0
  %tmp_59 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_1_val_0_0, i8 %col_buf_1_val_1_0, i8 %col_buf_1_val_2_0, i2 %row_assign_9_0_2_t)
  %src_kernel_win_1_val_2_0 = select i1 %tmp_17, i8 %tmp_59, i8 %col_buf_1_val_2_0
  br i1 %or_cond_i, label %.preheader.1, label %._crit_edge412.i.1_ifconv

; <label>:5                                       ; preds = %6
  br i1 %tmp_12, label %.preheader390.i.preheader.1, label %._crit_edge405.i.1_ifconv

; <label>:6                                       ; preds = %._crit_edge412.i.0_ifconv
  br i1 %icmp, label %5, label %borderInterpolate.exit422.i.1.0

.preheader390.i.preheader.1:                      ; preds = %5
  %right_border_buf_1_val_0_1_2 = load i8* %right_border_buf_1_val_0_1
  %right_border_buf_1_val_1_1_2 = load i8* %right_border_buf_1_val_1_1
  %right_border_buf_1_val_2_1_2 = load i8* %right_border_buf_1_val_2_1
  store i8 %k_buf_1_val_4_load, i8* %k_buf_1_val_5_addr, align 1
  store i8 %k_buf_1_val_3_load, i8* %k_buf_1_val_4_addr, align 1
  %tmp_85 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_1_V)
  store i8 %tmp_85, i8* %k_buf_1_val_3_addr, align 1
  store i8 %right_border_buf_1_val_2_1_2, i8* %right_border_buf_1_val_2_1_1
  store i8 %col_buf_1_val_2_0, i8* %right_border_buf_1_val_2_1
  store i8 %right_border_buf_1_val_1_1_2, i8* %right_border_buf_1_val_1_1_1
  store i8 %col_buf_1_val_1_0, i8* %right_border_buf_1_val_1_1
  store i8 %right_border_buf_1_val_0_1_2, i8* %right_border_buf_1_val_0_1_1
  store i8 %col_buf_1_val_0_0, i8* %right_border_buf_1_val_0_1
  br label %._crit_edge405.i.1_ifconv

"operator().exit465.i.1.0":                       ; preds = %borderInterpolate.exit422.i.1.0
  store i8 %tmp_84, i8* %k_buf_1_val_5_addr, align 1
  br label %._crit_edge407.i.1.0

._crit_edge407.i.1.0:                             ; preds = %borderInterpolate.exit422.i.1.0, %"operator().exit465.i.1.0"
  br i1 %tmp_116_0_1, label %"operator().exit465.i.1.1", label %._crit_edge407.i.1.1

"operator().exit465.i.1.1":                       ; preds = %._crit_edge407.i.1.0
  store i8 %tmp_84, i8* %k_buf_1_val_4_addr, align 1
  br label %._crit_edge407.i.1.1

._crit_edge407.i.1.1:                             ; preds = %"operator().exit465.i.1.1", %._crit_edge407.i.1.0
  br i1 %tmp_116_0_2, label %"operator().exit465.i.1.2", label %._crit_edge407.i.1.2

"operator().exit465.i.1.2":                       ; preds = %._crit_edge407.i.1.1
  store i8 %tmp_84, i8* %k_buf_1_val_3_addr, align 1
  br label %._crit_edge407.i.1.2

._crit_edge407.i.1.2:                             ; preds = %"operator().exit465.i.1.2", %._crit_edge407.i.1.1
  br label %._crit_edge405.i.1_ifconv

borderInterpolate.exit422.i.1.0:                  ; preds = %6
  %tmp_84 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_1_V)
  br i1 %tmp_14, label %"operator().exit465.i.1.0", label %._crit_edge407.i.1.0

.preheader.1:                                     ; preds = %._crit_edge405.i.1_ifconv
  %src_kernel_win_1_val_0_1_1_s = load i8* %src_kernel_win_1_val_0_1_1
  %src_kernel_win_1_val_1_1_1_s = load i8* %src_kernel_win_1_val_1_1_1
  %src_kernel_win_1_val_2_1_1_s = load i8* %src_kernel_win_1_val_2_1_1
  %OP1_V_1_0_cast = zext i8 %src_kernel_win_1_val_2_1_1_s to i9
  %tmp_160_1_0_2_cast = zext i8 %src_kernel_win_1_val_2_0 to i9
  %p_Val2_5_1_0_2 = sub i9 %tmp_160_1_0_2_cast, %OP1_V_1_0_cast
  %p_Val2_5_1_0_2_cast_cast = sext i9 %p_Val2_5_1_0_2 to i10
  %p_shl1 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_1_val_1_1_1_s, i1 false)
  %p_shl1_cast = zext i9 %p_shl1 to i10
  %p_Val2_1_1 = sub i10 0, %p_shl1_cast
  %tmp_160_1_1_cast = sext i10 %p_Val2_1_1 to i11
  %p_Val2_1_1_2 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_1_val_1_0, i1 false)
  %tmp_160_1_1_cast_61 = zext i9 %p_Val2_1_1_2 to i11
  %OP1_V_1_2_cast = zext i8 %src_kernel_win_1_val_0_1_1_s to i9
  %p_Val2_1_2 = sub i9 0, %OP1_V_1_2_cast
  %tmp_160_1_2_cast = sext i9 %p_Val2_1_2 to i11
  %tmp_160_1_2_2_cast_cast = zext i8 %src_kernel_win_1_val_0_0 to i10
  %tmp60 = add i11 %tmp_160_1_2_cast, %tmp_160_1_1_cast
  %tmp61 = add i10 %tmp_160_1_2_2_cast_cast, %p_Val2_5_1_0_2_cast_cast
  %tmp76_cast = sext i10 %tmp61 to i11
  %tmp62 = add i11 %tmp_160_1_1_cast_61, %tmp76_cast
  %p_Val2_4 = add i11 %tmp60, %tmp62
  %isneg_1 = call i1 @_ssdm_op_BitSelect.i1.i11.i32(i11 %p_Val2_4, i32 10)
  %p_Val2_5 = trunc i11 %p_Val2_4 to i8
  %tmp_63 = call i3 @_ssdm_op_PartSelect.i3.i11.i32.i32(i11 %p_Val2_4, i32 8, i32 10)
  %tmp_2_i_i1 = xor i1 %isneg_1, true
  %not_i_i_i1 = icmp ne i3 %tmp_63, 0
  %overflow_1 = and i1 %not_i_i_i1, %tmp_2_i_i1
  %p_mux_i_i30_cast = select i1 %tmp_2_i_i1, i8 -1, i8 0
  %tmp_i_i1 = or i1 %isneg_1, %overflow_1
  %p_Val2_10 = select i1 %tmp_i_i1, i8 %p_mux_i_i30_cast, i8 %p_Val2_5
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %p_dst_data_stream_1_V, i8 %p_Val2_10)
  br label %._crit_edge412.i.1_ifconv

._crit_edge412.i.2:                               ; preds = %.preheader.2, %._crit_edge405.i.2_ifconv
  %src_kernel_win_0_val_0_1_lo = load i8* %src_kernel_win_0_val_0_1
  %src_kernel_win_0_val_1_1_lo = load i8* %src_kernel_win_0_val_1_1
  %src_kernel_win_0_val_2_1_lo = load i8* %src_kernel_win_0_val_2_1
  %src_kernel_win_1_val_0_1_lo = load i8* %src_kernel_win_1_val_0_1
  %src_kernel_win_1_val_1_1_lo = load i8* %src_kernel_win_1_val_1_1
  %src_kernel_win_1_val_2_1_lo = load i8* %src_kernel_win_1_val_2_1
  %src_kernel_win_2_val_0_1_lo = load i8* %src_kernel_win_2_val_0_1
  %src_kernel_win_2_val_1_1_lo = load i8* %src_kernel_win_2_val_1_1
  %src_kernel_win_2_val_2_1_lo = load i8* %src_kernel_win_2_val_2_1
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @p_str1815, i32 %tmp_16)
  store i8 %src_kernel_win_2_val_2_1_lo, i8* %src_kernel_win_2_val_2_1_1
  store i8 %src_kernel_win_2_val_2_0, i8* %src_kernel_win_2_val_2_1
  store i8 %src_kernel_win_2_val_1_1_lo, i8* %src_kernel_win_2_val_1_1_1
  store i8 %src_kernel_win_2_val_1_0, i8* %src_kernel_win_2_val_1_1
  store i8 %src_kernel_win_2_val_0_1_lo, i8* %src_kernel_win_2_val_0_1_1
  store i8 %src_kernel_win_2_val_0_0, i8* %src_kernel_win_2_val_0_1
  store i8 %src_kernel_win_1_val_2_1_lo, i8* %src_kernel_win_1_val_2_1_1
  store i8 %src_kernel_win_1_val_2_0, i8* %src_kernel_win_1_val_2_1
  store i8 %src_kernel_win_1_val_1_1_lo, i8* %src_kernel_win_1_val_1_1_1
  store i8 %src_kernel_win_1_val_1_0, i8* %src_kernel_win_1_val_1_1
  store i8 %src_kernel_win_1_val_0_1_lo, i8* %src_kernel_win_1_val_0_1_1
  store i8 %src_kernel_win_1_val_0_0, i8* %src_kernel_win_1_val_0_1
  store i8 %src_kernel_win_0_val_2_1_lo, i8* %src_kernel_win_0_val_2_1_1
  store i8 %src_kernel_win_0_val_2_0, i8* %src_kernel_win_0_val_2_1
  store i8 %src_kernel_win_0_val_1_1_lo, i8* %src_kernel_win_0_val_1_1_1
  store i8 %src_kernel_win_0_val_1_0, i8* %src_kernel_win_0_val_1_1
  store i8 %src_kernel_win_0_val_0_1_lo, i8* %src_kernel_win_0_val_0_1_1
  store i8 %src_kernel_win_0_val_0_0, i8* %src_kernel_win_0_val_0_1
  br label %2

._crit_edge405.i.2_ifconv:                        ; preds = %._crit_edge407.i.2.2, %.preheader390.i.preheader.2, %7, %._crit_edge412.i.1_ifconv
  %tmp_68 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_2_val_0_0, i8 %col_buf_2_val_1_0, i8 %col_buf_2_val_2_0, i2 %row_assign_9)
  %src_kernel_win_2_val_0_0 = select i1 %tmp_17, i8 %tmp_68, i8 %col_buf_2_val_0_0
  %tmp_69 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_2_val_0_0, i8 %col_buf_2_val_1_0, i8 %col_buf_2_val_2_0, i2 %row_assign_9_0_1_t)
  %src_kernel_win_2_val_1_0 = select i1 %tmp_17, i8 %tmp_69, i8 %col_buf_2_val_1_0
  %tmp_70 = call i8 @_ssdm_op_Mux.ap_auto.3i8.i2(i8 %col_buf_2_val_0_0, i8 %col_buf_2_val_1_0, i8 %col_buf_2_val_2_0, i2 %row_assign_9_0_2_t)
  %src_kernel_win_2_val_2_0 = select i1 %tmp_17, i8 %tmp_70, i8 %col_buf_2_val_2_0
  br i1 %or_cond_i, label %.preheader.2, label %._crit_edge412.i.2

; <label>:7                                       ; preds = %8
  br i1 %tmp_12, label %.preheader390.i.preheader.2, label %._crit_edge405.i.2_ifconv

; <label>:8                                       ; preds = %._crit_edge412.i.1_ifconv
  br i1 %icmp, label %7, label %borderInterpolate.exit422.i.2.0

.preheader390.i.preheader.2:                      ; preds = %7
  store i8 %k_buf_2_val_4_load, i8* %k_buf_2_val_5_addr, align 1
  store i8 %k_buf_2_val_3_load, i8* %k_buf_2_val_4_addr, align 1
  %tmp_90 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_2_V)
  store i8 %tmp_90, i8* %k_buf_2_val_3_addr, align 1
  store i8 %col_buf_2_val_0_0, i8* %right_border_buf_2_val_0_1_1
  store i8 %right_border_buf_2_val_0_1_2, i8* %right_border_buf_2_val_0_1
  store i8 %col_buf_2_val_1_0, i8* %right_border_buf_2_val_1_1_1
  store i8 %right_border_buf_2_val_1_1_2, i8* %right_border_buf_2_val_1_1
  store i8 %col_buf_2_val_2_0, i8* %right_border_buf_2_val_2_1_1
  store i8 %right_border_buf_2_val_2_1_2, i8* %right_border_buf_2_val_2_1
  br label %._crit_edge405.i.2_ifconv

"operator().exit465.i.2.0":                       ; preds = %borderInterpolate.exit422.i.2.0
  store i8 %tmp_89, i8* %k_buf_2_val_5_addr, align 1
  br label %._crit_edge407.i.2.0

._crit_edge407.i.2.0:                             ; preds = %borderInterpolate.exit422.i.2.0, %"operator().exit465.i.2.0"
  br i1 %tmp_116_0_1, label %"operator().exit465.i.2.1", label %._crit_edge407.i.2.1

"operator().exit465.i.2.1":                       ; preds = %._crit_edge407.i.2.0
  store i8 %tmp_89, i8* %k_buf_2_val_4_addr, align 1
  br label %._crit_edge407.i.2.1

._crit_edge407.i.2.1:                             ; preds = %"operator().exit465.i.2.1", %._crit_edge407.i.2.0
  br i1 %tmp_116_0_2, label %"operator().exit465.i.2.2", label %._crit_edge407.i.2.2

"operator().exit465.i.2.2":                       ; preds = %._crit_edge407.i.2.1
  store i8 %tmp_89, i8* %k_buf_2_val_3_addr, align 1
  br label %._crit_edge407.i.2.2

._crit_edge407.i.2.2:                             ; preds = %"operator().exit465.i.2.2", %._crit_edge407.i.2.1
  br label %._crit_edge405.i.2_ifconv

borderInterpolate.exit422.i.2.0:                  ; preds = %8
  %tmp_89 = call i8 @_ssdm_op_Read.ap_fifo.volatile.i8P(i8* %p_src_data_stream_2_V)
  br i1 %tmp_14, label %"operator().exit465.i.2.0", label %._crit_edge407.i.2.0

.preheader.2:                                     ; preds = %._crit_edge405.i.2_ifconv
  %src_kernel_win_2_val_0_1_1_s = load i8* %src_kernel_win_2_val_0_1_1
  %src_kernel_win_2_val_1_1_1_s = load i8* %src_kernel_win_2_val_1_1_1
  %src_kernel_win_2_val_2_1_1_s = load i8* %src_kernel_win_2_val_2_1_1
  %OP1_V_2_0_cast = zext i8 %src_kernel_win_2_val_2_1_1_s to i9
  %tmp_160_2_0_2_cast = zext i8 %src_kernel_win_2_val_2_0 to i9
  %p_Val2_5_2_0_2 = sub i9 %tmp_160_2_0_2_cast, %OP1_V_2_0_cast
  %p_Val2_5_2_0_2_cast_cast = sext i9 %p_Val2_5_2_0_2 to i10
  %p_shl2 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_2_val_1_1_1_s, i1 false)
  %p_shl2_cast = zext i9 %p_shl2 to i10
  %p_Val2_2_1 = sub i10 0, %p_shl2_cast
  %tmp_160_2_1_cast = sext i10 %p_Val2_2_1 to i11
  %p_Val2_2_1_2 = call i9 @_ssdm_op_BitConcatenate.i9.i8.i1(i8 %src_kernel_win_2_val_1_0, i1 false)
  %tmp_160_2_1_cast_62 = zext i9 %p_Val2_2_1_2 to i11
  %OP1_V_2_2_cast = zext i8 %src_kernel_win_2_val_0_1_1_s to i9
  %p_Val2_2_2 = sub i9 0, %OP1_V_2_2_cast
  %tmp_160_2_2_cast = sext i9 %p_Val2_2_2 to i11
  %tmp_160_2_2_2_cast_cast = zext i8 %src_kernel_win_2_val_0_0 to i10
  %tmp71 = add i11 %tmp_160_2_2_cast, %tmp_160_2_1_cast
  %tmp72 = add i10 %tmp_160_2_2_2_cast_cast, %p_Val2_5_2_0_2_cast_cast
  %tmp81_cast = sext i10 %tmp72 to i11
  %tmp73 = add i11 %tmp_160_2_1_cast_62, %tmp81_cast
  %p_Val2_s = add i11 %tmp71, %tmp73
  %isneg_2 = call i1 @_ssdm_op_BitSelect.i1.i11.i32(i11 %p_Val2_s, i32 10)
  %p_Val2_7 = trunc i11 %p_Val2_s to i8
  %tmp_73 = call i3 @_ssdm_op_PartSelect.i3.i11.i32.i32(i11 %p_Val2_s, i32 8, i32 10)
  %tmp_2_i_i2 = xor i1 %isneg_2, true
  %not_i_i_i2 = icmp ne i3 %tmp_73, 0
  %overflow_2 = and i1 %not_i_i_i2, %tmp_2_i_i2
  %p_mux_i_i39_cast = select i1 %tmp_2_i_i2, i8 -1, i8 0
  %tmp_i_i2 = or i1 %isneg_2, %overflow_2
  %p_Val2_11 = select i1 %tmp_i_i2, i8 %p_mux_i_i39_cast, i8 %p_Val2_7
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %p_dst_data_stream_2_V, i8 %p_Val2_11)
  br label %._crit_edge412.i.2

; <label>:9                                       ; preds = %2
  %empty_63 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str1814, i32 %tmp_11)
  br label %0

"filter<32, 32, ap_int<8>, int, 512, 512, 3, 3>.exit": ; preds = %0
  ret void

branch0:                                          ; preds = %arrayctor.loop1.i
  br label %arrayctor.loop1.i354_ifconv

branch1:                                          ; preds = %arrayctor.loop1.i
  br label %arrayctor.loop1.i354_ifconv

branch2:                                          ; preds = %arrayctor.loop1.i
  br label %arrayctor.loop1.i354_ifconv
}

define internal fastcc void @image_filter_Block__proc(i11* %img_0_rows_V_out, i11* %img_0_cols_V_out) {
entry:
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_cols_V_out, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_0_rows_V_out, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void @_ssdm_op_Write.ap_fifo.i11P(i11* %img_0_rows_V_out, i11 512)
  call void @_ssdm_op_Write.ap_fifo.i11P(i11* %img_0_cols_V_out, i11 512)
  ret void
}

define internal fastcc void @"image_filter_AXIvideo2Mat<32, 512, 512, 32>"(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, i11* nocapture %img_rows_V, i11* nocapture %img_cols_V, i8* %img_data_stream_0_V, i8* %img_data_stream_1_V, i8* %img_data_stream_2_V, i11* %img_rows_V_out, i11* %img_cols_V_out) {
entry:
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_2_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_1_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i8* %img_data_stream_0_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_cols_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_rows_V, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  %img_rows_V_read = call i11 @_ssdm_op_Read.ap_fifo.i11P(i11* %img_rows_V)
  %img_cols_V_read = call i11 @_ssdm_op_Read.ap_fifo.i11P(i11* %img_cols_V)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_rows_V_out, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void @_ssdm_op_Write.ap_fifo.i11P(i11* %img_rows_V_out, i11 %img_rows_V_read)
  call void (...)* @_ssdm_op_SpecInterface(i11* %img_cols_V_out, [8 x i8]* @ap_fifo_str, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str)
  call void @_ssdm_op_Write.ap_fifo.i11P(i11* %img_cols_V_out, i11 %img_cols_V_read)
  call void (...)* @_ssdm_op_SpecInterface(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V, [5 x i8]* @p_str1805, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str1806, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807, [1 x i8]* @p_str1807) nounwind
  br label %._crit_edge188.i

._crit_edge188.i:                                 ; preds = %._crit_edge188.i, %entry
  call void (...)* @_ssdm_op_SpecLoopName([20 x i8]* @p_str1846) nounwind
  %tmp_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([20 x i8]* @p_str1846)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 0, i32 0, [1 x i8]* @p_str1807) nounwind
  %empty = call { i32, i4, i4, i1, i1, i1, i1 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V)
  %tmp_data_V = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty, 0
  %tmp_user_V = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty, 3
  %tmp_last_V = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty, 4
  %empty_64 = call i32 (...)* @_ssdm_op_SpecRegionEnd([20 x i8]* @p_str1846, i32 %tmp_i)
  br i1 %tmp_user_V, label %.preheader187.i.preheader, label %._crit_edge188.i

.preheader187.i.preheader:                        ; preds = %._crit_edge188.i
  %sof_1_i = alloca i1
  store i1 true, i1* %sof_1_i
  br label %.preheader187.i

.preheader187.i:                                  ; preds = %.preheader187.i.preheader, %5
  %axi_last_V1_i = phi i1 [ %axi_last_V_3_i, %5 ], [ %tmp_last_V, %.preheader187.i.preheader ]
  %axi_data_V1_i = phi i32 [ %axi_data_V_3_i, %5 ], [ %tmp_data_V, %.preheader187.i.preheader ]
  %p_i = phi i10 [ %i_V, %5 ], [ 0, %.preheader187.i.preheader ]
  %p_cast_cast_i = zext i10 %p_i to i11
  %exitcond6_i = icmp eq i11 %p_cast_cast_i, %img_rows_V_read
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 0, i64 512, i64 0)
  %i_V = add i10 %p_i, 1
  br i1 %exitcond6_i, label %.exit, label %0

; <label>:0                                       ; preds = %.preheader187.i
  call void (...)* @_ssdm_op_SpecLoopName([12 x i8]* @p_str1814) nounwind
  %tmp_118_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([12 x i8]* @p_str1814)
  br label %1

; <label>:1                                       ; preds = %._crit_edge189.i, %0
  %eol = phi i1 [ %axi_last_V1_i, %0 ], [ %axi_last_V_2_i, %._crit_edge189.i ]
  %axi_data_V_1_i = phi i32 [ %axi_data_V1_i, %0 ], [ %p_Val2_s, %._crit_edge189.i ]
  %p_4_i = phi i10 [ 0, %0 ], [ %j_V, %._crit_edge189.i ]
  %eol_i = phi i1 [ false, %0 ], [ %axi_last_V_2_i, %._crit_edge189.i ]
  %p_4_cast_cast_i = zext i10 %p_4_i to i11
  %exitcond7_i = icmp eq i11 %p_4_cast_cast_i, %img_cols_V_read
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 0, i64 512, i64 0)
  %j_V = add i10 %p_4_i, 1
  br i1 %exitcond7_i, label %.preheader.i, label %2

; <label>:2                                       ; preds = %1
  %sof_1_i_load = load i1* %sof_1_i
  call void (...)* @_ssdm_op_SpecLoopName([11 x i8]* @p_str1815) nounwind
  %tmp_119_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([11 x i8]* @p_str1815)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1807) nounwind
  %brmerge_i = or i1 %sof_1_i_load, %eol_i
  br i1 %brmerge_i, label %._crit_edge189.i, label %3

; <label>:3                                       ; preds = %2
  %empty_65 = call { i32, i4, i4, i1, i1, i1, i1 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V)
  %tmp_data_V_1 = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty_65, 0
  %tmp_last_V_1 = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty_65, 4
  br label %._crit_edge189.i

._crit_edge189.i:                                 ; preds = %3, %2
  %axi_last_V_2_i = phi i1 [ %tmp_last_V_1, %3 ], [ %eol, %2 ]
  %p_Val2_s = phi i32 [ %tmp_data_V_1, %3 ], [ %axi_data_V_1_i, %2 ]
  %tmp = trunc i32 %p_Val2_s to i8
  %tmp_31 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %p_Val2_s, i32 8, i32 15)
  %tmp_32 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %p_Val2_s, i32 16, i32 23)
  %tmp_124_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([13 x i8]* @p_str1848)
  call void (...)* @_ssdm_op_SpecProtocol(i32 0, [1 x i8]* @p_str1807) nounwind
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %img_data_stream_0_V, i8 %tmp)
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %img_data_stream_1_V, i8 %tmp_31)
  call void @_ssdm_op_Write.ap_fifo.volatile.i8P(i8* %img_data_stream_2_V, i8 %tmp_32)
  %empty_66 = call i32 (...)* @_ssdm_op_SpecRegionEnd([13 x i8]* @p_str1848, i32 %tmp_124_i)
  %empty_67 = call i32 (...)* @_ssdm_op_SpecRegionEnd([11 x i8]* @p_str1815, i32 %tmp_119_i)
  store i1 false, i1* %sof_1_i
  br label %1

.preheader.i:                                     ; preds = %1, %4
  %axi_last_V_3_i = phi i1 [ %tmp_last_V_2, %4 ], [ %eol, %1 ]
  %axi_data_V_3_i = phi i32 [ %tmp_data_V_2, %4 ], [ %axi_data_V_1_i, %1 ]
  %eol_2_i = phi i1 [ %tmp_last_V_2, %4 ], [ %eol_i, %1 ]
  br i1 %eol_2_i, label %5, label %4

; <label>:4                                       ; preds = %.preheader.i
  call void (...)* @_ssdm_op_SpecLoopName([18 x i8]* @p_str1847) nounwind
  %tmp_120_i = call i32 (...)* @_ssdm_op_SpecRegionBegin([18 x i8]* @p_str1847)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1807) nounwind
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 0, i32 0, [1 x i8]* @p_str1807) nounwind
  %empty_68 = call { i32, i4, i4, i1, i1, i1, i1 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i1P.i1P.i1P.i1P(i32* %AXI_video_strm_V_data_V, i4* %AXI_video_strm_V_keep_V, i4* %AXI_video_strm_V_strb_V, i1* %AXI_video_strm_V_user_V, i1* %AXI_video_strm_V_last_V, i1* %AXI_video_strm_V_id_V, i1* %AXI_video_strm_V_dest_V)
  %tmp_data_V_2 = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty_68, 0
  %tmp_last_V_2 = extractvalue { i32, i4, i4, i1, i1, i1, i1 } %empty_68, 4
  %empty_69 = call i32 (...)* @_ssdm_op_SpecRegionEnd([18 x i8]* @p_str1847, i32 %tmp_120_i)
  br label %.preheader.i

; <label>:5                                       ; preds = %.preheader.i
  %empty_70 = call i32 (...)* @_ssdm_op_SpecRegionEnd([12 x i8]* @p_str1814, i32 %tmp_118_i)
  br label %.preheader187.i

.exit:                                            ; preds = %.preheader187.i
  ret void
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"video_in.V.data.V", metadata !5, metadata !"uint32", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 3, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"video_in.V.keep.V", metadata !5, metadata !"uint4", i32 0, i32 3}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 3, metadata !17}
!17 = metadata !{metadata !18}
!18 = metadata !{metadata !"video_in.V.strb.V", metadata !5, metadata !"uint4", i32 0, i32 3}
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 0, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"video_in.V.user.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 0, metadata !25}
!25 = metadata !{metadata !26}
!26 = metadata !{metadata !"video_in.V.last.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!27 = metadata !{metadata !28}
!28 = metadata !{i32 0, i32 0, metadata !29}
!29 = metadata !{metadata !30}
!30 = metadata !{metadata !"video_in.V.id.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!31 = metadata !{metadata !32}
!32 = metadata !{i32 0, i32 0, metadata !33}
!33 = metadata !{metadata !34}
!34 = metadata !{metadata !"video_in.V.dest.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!35 = metadata !{metadata !36}
!36 = metadata !{i32 0, i32 31, metadata !37}
!37 = metadata !{metadata !38}
!38 = metadata !{metadata !"video_out.V.data.V", metadata !5, metadata !"uint32", i32 0, i32 31}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 0, i32 3, metadata !41}
!41 = metadata !{metadata !42}
!42 = metadata !{metadata !"video_out.V.keep.V", metadata !5, metadata !"uint4", i32 0, i32 3}
!43 = metadata !{metadata !44}
!44 = metadata !{i32 0, i32 3, metadata !45}
!45 = metadata !{metadata !46}
!46 = metadata !{metadata !"video_out.V.strb.V", metadata !5, metadata !"uint4", i32 0, i32 3}
!47 = metadata !{metadata !48}
!48 = metadata !{i32 0, i32 0, metadata !49}
!49 = metadata !{metadata !50}
!50 = metadata !{metadata !"video_out.V.user.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!51 = metadata !{metadata !52}
!52 = metadata !{i32 0, i32 0, metadata !53}
!53 = metadata !{metadata !54}
!54 = metadata !{metadata !"video_out.V.last.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!55 = metadata !{metadata !56}
!56 = metadata !{i32 0, i32 0, metadata !57}
!57 = metadata !{metadata !58}
!58 = metadata !{metadata !"video_out.V.id.V", metadata !5, metadata !"uint1", i32 0, i32 0}
!59 = metadata !{metadata !60}
!60 = metadata !{i32 0, i32 0, metadata !61}
!61 = metadata !{metadata !62}
!62 = metadata !{metadata !"video_out.V.dest.V", metadata !5, metadata !"uint1", i32 0, i32 0}
