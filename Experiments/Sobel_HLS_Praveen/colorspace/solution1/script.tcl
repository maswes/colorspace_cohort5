############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project colorspace
set_top image_filter
add_files colorspace/hls_sobel.cpp
add_files colorspace/hls_solbel.h
add_files -tb colorspace/hls_video_test.cpp
open_solution "solution1"
set_part {xc7k160tfbg484-2}
create_clock -period 10 -name default
#source "./colorspace/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
