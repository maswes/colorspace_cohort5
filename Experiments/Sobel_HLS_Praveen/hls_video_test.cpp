
#include "hls_solbel.h"
#include <opencv2/opencv.hpp>
#include <hls_opencv.h>
void opencv_image_filter(IplImage *src, IplImage *dst);
int main (int argc, char** argv)
{
// Load data in OpenCV image format
char* INPUT_IMAGE, OUTPUT_IMAGE;
IplImage* src = cvLoadImage("lena_gray.bmp");
IplImage* dst = cvCreateImage(cvGetSize(src), src->depth, src->nChannels);

AXI_STREAM src_axi, dst_axi;
// Convert OpenCV format to AXI4 Stream format
IplImage2AXIvideo(src, src_axi);
//IplImage2AXIvideo(NULL, NULL);
// Call the function to be synthesized
//image_filter(src_axi, dst_axi, src->height, src->width);
image_filter(src_axi, dst_axi);
// Convert the AXI4 Stream data to OpenCV format
AXIvideo2IplImage(dst_axi, dst);
// Standard OpenCV image functions
cvSaveImage("lena_gray_out.bmp", dst);
opencv_image_filter(src, dst);
cvSaveImage("lena_gray_out_GOLDEN.bmp", dst);
cvReleaseImage(&src);
cvReleaseImage(&dst);
char tempbuf[2000];
sprintf(tempbuf, "diff --brief -w %s %s", "lena_gray_out.bmp", "lena_gray_out_GOLDEN.bmp");
int ret = system(tempbuf);
if (ret != 0) {
printf("Test Failed!\n");
ret = 1;
} else {
printf("Test Passed!\n");
}
return ret;
}

void opencv_image_filter(IplImage *src, IplImage *dst)
{
cvCopy(src, dst);
}


