#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <vector>

using namespace cv;
using namespace std;

int _img_w, _img_h;
int* _accu,_accu_h,_accu_w;
void custom_sobel(Mat img_i, Mat& pout);
int Hough_Transform(unsigned char* img_data, int w, int h);
std::vector< std::pair< std::pair<int, int>, std::pair<int, int> > > Hough_GetLines(int threshold);


#define DEG2RAD (4.0 *atan(1.0)/180.0)

int main(int argc, char** argv)
{
 //const char* filename = argc >= 2 ? argv[1] : "pic1.jpg";

	string filename = "leena.jpg";
	char key=0;
 Mat frame = imread(filename, 0);
do{
 if(frame.empty())
 {
     //help();
     cout << "can not open " << filename << endl;
     return -1;
 }

 int row = frame.rows;
 int col = frame.cols;
 printf("\n row = %d, col=%d", row, col);

 Mat dst, cdst;
 dst =  Mat(row-1, col-1, DataType<float>::type);
custom_sobel(frame, dst);
 //Canny(frame, dst, 50, 200, 3);

 imshow("After Sobel", dst);
 cvtColor(dst, cdst, CV_GRAY2BGR);

 imshow("source", frame);
 //imshow("detected lines", cdst);
	
 key = waitKey(100);
}while(key != 'q');
 return 0;
}

int Hough_Transform(int* img_data, int w, int h)  
{  
    _img_w = w;  
    _img_h = h;  

    //Create the accu  
    double hough_h = ((sqrt(2.0) * (double)(h>w?h:w)) / 2.0);  
    _accu_h = hough_h * 2.0; // -r -> +r  
    _accu_w = 180;  

     _accu = (int*)calloc(_accu_h * _accu_w, sizeof( int));  

     double center_x = w/2;  
     double center_y = h/2;  


     for(int y=0;y<h;y++)  
     {  
          for(int x=0;x<w;x++)  
          {  
               if( img_data[ (y*w) + x] > 250 )  
               {  
                    for(int t=0;t<180;t++)  
                    {  
                         double r = ( ((double)x - center_x) * cos((double)t * DEG2RAD)) + (((double)y - center_y) * sin((double)t * DEG2RAD));  
                         _accu[ (int)((round(r + hough_h) * 180.0)) + t]++;  
                    }  
               }  
          }  
     }  

     return 0;  
}  
std::vector< std::pair< std::pair<int, int>, std::pair<int, int> > > Hough_GetLines(int threshold)  
{  
    std::vector< std::pair< std::pair<int, int>, std::pair<int, int> > > lines;  

    if(_accu == 0)  
         return lines;  

    for(int r=0;r<_accu_h;r++)  
    {  
          for(int t=0;t<_accu_w;t++)  
          {  
               if((int)_accu[(r*_accu_w) + t] >= threshold)  
               {  
                    //Is this point a local maxima (9x9)  
                    int max = _accu[(r*_accu_w) + t];  
                    for(int ly=-4;ly<=4;ly++)  
                    {  
                         for(int lx=-4;lx<=4;lx++)  
                         {  
                              if( (ly+r>=0 && ly+r<_accu_h) && (lx+t>=0 && lx+t<_accu_w) )  
                              {  
                                   if( (int)_accu[( (r+ly)*_accu_w) + (t+lx)] > max )  
                                   {  
                                        max = _accu[( (r+ly)*_accu_w) + (t+lx)];  
                                        ly = lx = 5;  
                                   }  
                              }  
                         }  
                    }  
                    if(max > (int)_accu[(r*_accu_w) + t])  
                         continue;  


                    int x1, y1, x2, y2;  
                    x1 = y1 = x2 = y2 = 0;  

                    if(t >= 45 && t <= 135)  
                    {  
                         //y = (r - x cos(t)) / sin(t)  
                         x1 = 0;  
                         y1 = ((double)(r-(_accu_h/2)) - ((x1 - (_img_w/2) ) * cos(t * DEG2RAD))) / sin(t * DEG2RAD) + (_img_h / 2);  
                         x2 = _img_w - 0;  
                         y2 = ((double)(r-(_accu_h/2)) - ((x2 - (_img_w/2) ) * cos(t * DEG2RAD))) / sin(t * DEG2RAD) + (_img_h / 2);  
                    }  
                    else  
                    {  
                         //x = (r - y sin(t)) / cos(t);  
                         y1 = 0;  
                         x1 = ((double)(r-(_accu_h/2)) - ((y1 - (_img_h/2) ) * sin(t * DEG2RAD))) / cos(t * DEG2RAD) + (_img_w / 2);  
                         y2 = _img_h - 0;  
                         x2 = ((double)(r-(_accu_h/2)) - ((y2 - (_img_h/2) ) * sin(t * DEG2RAD))) / cos(t * DEG2RAD) + (_img_w / 2);  
                    }  

                    lines.push_back(std::pair< std::pair<int, int>, std::pair<int, int> >(std::pair<int, int>(x1,y1), std::pair<int, int>(x2,y2)));  

               }  
          }  
     }  

     std::cout << "lines: " << lines.size() << " " << threshold << std::endl;  
     return lines;  
}  
/*
void main()
{
		 cv::Mat img_edge;  
2:       cv::Mat img_blur;  
3:    
4:       cv::Mat img_ori = cv::imread( file_path, 1 );  
5:       cv::blur( img_ori, img_blur, cv::Size(5,5) );  
6:       cv::Canny(img_blur, img_edge, 100, 150, 3);  
7:    
8:       int w = img_edge.cols;  
9:       int h = img_edge.rows;  
	 //Search the accumulator  
2:            std::vector< std::pair< std::pair<int, int>, std::pair<int, int> > > lines = hough.GetLines(threshold);  
3:    
4:            //Draw the results  
5:            std::vector< std::pair< std::pair<int, int>, std::pair<int, int> > >::iterator it;  
6:            for(it=lines.begin();it!=lines.end();it++)  
7:            {  
8:                 cv::line(img_res, cv::Point(it->first.first, it->first.second), cv::Point(it->second.first, it->second.second), cv::Scalar( 0, 0, 255), 2, 8);  
9:            }  
10:    
11:            //Visualize all  
12:            int aw, ah, maxa;  
13:            aw = ah = maxa = 0;  
14:            const unsigned int* accu = hough.GetAccu(&aw, &ah);  
15:    
16:            for(int p=0;p<(ah*aw);p++)  
17:            {  
18:                 if(accu[p] > maxa)  
19:                      maxa = accu[p];  
20:            }  
21:            double contrast = 1.0;  
22:            double coef = 255.0 / (double)maxa * contrast;  
23:    
24:            cv::Mat img_accu(ah, aw, CV_8UC3);  
25:            for(int p=0;p<(ah*aw);p++)  
26:            {  
27:                 unsigned char c = (double)accu[p] * coef < 255.0 ? (double)accu[p] * coef : 255.0;  
28:                 img_accu.data[(p*3)+0] = 255;  
29:                 img_accu.data[(p*3)+1] = 255-c;  
30:                 img_accu.data[(p*3)+2] = 255-c;  
31:            }  
32:    
33:    
34:            cv::imshow(CW_IMG_ORIGINAL, img_res);  
35:            cv::imshow(CW_IMG_EDGE, img_edge);  
36:            cv::imshow(CW_ACCUMULATOR, img_accu);
}
}*/
