#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
using namespace cv;
using namespace std;

/*unoptimized sobel*/
void custom_sobel(Mat img_i, Mat& pout){

    /*

     * your code goes here

     */
   
  long long t;
  int x, y, i, j;
  float acc=0.f,acch=0.f,accv=0.f,accb=0.f;
  float min, max;

  //static Mat img_i;
  static Mat img_h;
  static Mat img_v;
  static Mat img_b;
  static Mat img_o;

  int row = img_i.rows;
  int col = img_i.cols;

  printf("\n row = %d and col = %d", row, col);
  //cout << "Sobel called";


  //img_i =  Mat(row, col, CV_32FC1, 0);
  img_h =  Mat(row-1, col-1, DataType<float>::type);
  img_v =  Mat(row-1, col-1, DataType<float>::type);
  img_b =  Mat(row-1, col-1, DataType<float>::type); 	
  img_o =  Mat(row-1, col-1, DataType<float>::type);
  //pout =  Mat(row-1, col-1, DataType<float>::type);
 
 static const float filter_arr[3][3] = {{-1, -2, -1},
					 { 0,  0,  0},
					 { 1,  2,  1}};


  for (y = 1; y < row-1; y++) {
    for (x = 1; x < col-1; x++) {
      acch = 0.f;
      for(i=0; i<=2; i++) {
        for(j=0; j<=2; j++) {
          acch += img_i.at<unsigned char>(y+i-1,x+j-1) * filter_arr[i][j];
        }
      }  
      img_h.at<float>(y-1,x-1) = acch; 

    }
  }

printf("\n After first loop");

  for (y = 1; y < row-1; y++) {
    for (x = 1; x < col-1; x++) {
      accv = 0.f;
      for(i=0; i<=2; i++) {

        for(j=0; j<=2; j++) {
          accv += img_i.at<unsigned char>(y+i-1,x+j-1) * filter_arr[j][i];
        }

      }  
      img_v.at<float>(y-1,x-1) = accv; 
    }
  }


  //printf("Time: %lld\n", utime() - t);

  min = 100000000; 
  max = -100000000;

  for (y = 0; y < row-2; y++) {
    for (x = 0; x < col-2; x++) {
      acc = (img_h.at<float>(y,x)*img_h.at<float>(y,x)) + (img_v.at<float>(y,x)*img_v.at<float>(y,x));

      acc = sqrtf(acc);
      img_b.at<float>(y,x) = acc;

      if(acc > max) max = acc;
      if(acc < min) min = acc;

    }
  }

  printf("MAX = %f, MIN=%f", max,min);

 // printf("Time: %lld\n", utime() - t);

  for (y = 0; y < row-2; y++) {
    for (x = 0; x < col-2; x++) {
      pout.at<unsigned char>(y,x) = (unsigned char) (256.f * (img_b.at<float>(y,x) - min) / (max - min));
    }
  }

  cv::imshow("smothed",pout);

}
