#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "iostream"

#define mbh     8
#define mbw     8 
#define WIDTH   320
#define HEIGHT  256

using namespace cv;
using namespace std;
 

int main( )
{
	VideoCapture cap(0);
	Mat frame, gray;
	char key=0;
	while(key != 'q'){
	cap >> frame;
	cvtColor(frame, gray, CV_BGR2GRAY);
	resize(gray, gray, Size(WIDTH, HEIGHT));

	Mat dst, cdst;
    	Canny(gray, dst, 50, 200, 3); 
    	imshow("After Canny", dst);
    	cvtColor(dst, cdst, CV_GRAY2BGR); 
 
	vector<Vec2f> lines;
	// detect lines
	HoughLines(dst, lines, 1, CV_PI/180, 150, 0, 0 );

	// draw lines
	for( size_t i = 0; i < lines.size(); i++ )
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
	}
 
    	imshow("source", gray);
    	imshow("detected lines", cdst);
 
  
    	key = waitKey(50);
	printf("Time: \n");
    }

    return 0;
} 
