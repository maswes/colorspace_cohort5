/*
(c) Copyright 2013 - 2016 Xilinx, Inc. All rights reserved.

This file contains confidential and proprietary information of Xilinx, Inc. and
is protected under U.S. and international copyright and other intellectual
property laws.

DISCLAIMER
This disclaimer is not a license and does not grant any rights to the materials
distributed herewith. Except as otherwise provided in a valid license issued to
you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR
FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable (whether
in contract or tort, including negligence, or under any other theory of
liability) for any loss or damage of any kind or nature related to, arising
under or in connection with these materials, including for any direct, or any
indirect, special, incidental, or consequential loss or damage (including loss
of data, profits, goodwill, or any type of loss or damage suffered as a result
of any action brought by a third party) even if such damage or loss was
reasonably foreseeable or Xilinx had been advised of the possibility of the
same.

CRITICAL APPLICATIONS
Xilinx products are not designed or intended to be fail-safe, or for use in any
application requiring fail-safe performance, such as life-support or safety
devices or systems, Class III medical devices, nuclear facilities, applications
related to the deployment of airbags, or any other applications that could lead
to death, personal injury, or severe property or environmental damage
(individually and collectively, "Critical Applications"). Customer assumes the
sole risk and liability of any use of Xilinx products in Critical Applications,
subject only to applicable laws and regulations governing limitations on product
liability.

THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
ALL TIMES.
*/

#include <iostream>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define USE_OPENCV
//#include "sds_lib.h"
#include "sobel.h"
#include <opencv2/opencv.hpp>
#include  "hls_opencv.h"
using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
	   RGB_IMAGE src_hls(HEIGHT,   WIDTH);
	   RGB_IMAGE dst_hls(HEIGHT,   WIDTH);

	  printf("\n Going to load Video");

		//string filename = "TrafficLanes.mp4";
		 //cv::VideoCapture cap(filename);
    //VideoCapture cap(0);
	  string imageName = "test.jpg";

	  if(argv[1] == "")
		  ;
	  else
		  imageName = argv[1];

	  Mat image;
	  image = imread( imageName, 1 );

	  if( !image.data )
	  {
		printf( " No image data \n " );
		return -1;
	  }
	  else
		  printf( " No of elements in image row = %d and cols = %d \n ", image.rows, image.cols );
	Mat frame, gray;
		 	//sobel_wrapper(src_hls, dst_hls);
		 	printf("\n DONE Going to load Video");
		 	//char key=0;
		 	//while(key != 'q'){
		 	//cap >> frame;

		 	printf("\n Going frame by frame");
		 	cvtColor(image, gray, CV_RGB2GRAY);
		 	resize(gray, gray, Size(WIDTH, WIDTH));

		 	cvMat2hlsMat( gray, src_hls);
		 	Mat dst(WIDTH, HEIGHT, CV_8UC1); //, cdst;

		 	printf("\n Before calling Sobel");
		 	sobel_wrapper(src_hls, dst_hls);

		 	printf("\n After calling Sobel");
		 	hls::Polar_< int, int > lines[100];
		 	hough_wrapper(dst_hls, lines);
		 	hlsMat2cvMat(dst_hls, dst);

		 	printf("\n Before writing image");
		 	imwrite("test_afterSobel.jpg", dst); //, IMWRITE_JPEG_QUALITY );
		     //imshow("After Canny", dst);
		 	//}





     return 0; //(test_passed ? -1 : 0);
}
