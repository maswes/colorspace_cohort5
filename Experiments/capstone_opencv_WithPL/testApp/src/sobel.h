/*
 * sobel.h
 *
 *  Created on: Apr 16, 2017
 *      Author: C5Vive
 */

#ifndef SOBEL_H_
#define SOBEL_H_
#include  "hls_video.h"
#include <hls_stream.h>
#include "ap_axi_sdata.h"
#include "ap_int.h"
#define WIDTH  512 //220
#define HEIGHT 512 //220
typedef hls::Mat<HEIGHT, WIDTH, HLS_8UC1> RGB_IMAGE;
//typedef hls::stream<ap_axiu<W,1,1,1> >
//#pragma SDS data access_pattern(INPUT_STREAM:SEQUENTIAL, OUTPUT_STREAM:SEQUENTIAL)
//void sobel_wrapper(RGB_IMAGE INPUT_STREAM, RGB_IMAGE OUTPUT_STREAM);
void sobel_wrapper(RGB_IMAGE& INPUT_STREAM, RGB_IMAGE& OUTPUT_STREAM);

//template<unsigned int max>
void hough_wrapper(RGB_IMAGE& INPUT_STREAM, hls::Polar_< int, int > (&lines)[100]);



#endif /* SOBEL_H_ */
